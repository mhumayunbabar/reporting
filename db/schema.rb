# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170920023011) do

  create_table "admins", force: :cascade do |t|
    t.string   "email"
    t.string   "password"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "district_id"
    t.integer  "tehsil_id"
    t.integer  "state",       default: 1
  end

  create_table "admins_roles", id: false, force: :cascade do |t|
    t.integer "admin_id"
    t.integer "role_id"
  end

  add_index "admins_roles", ["admin_id", "role_id"], name: "index_admins_roles_on_admin_id_and_role_id"

  create_table "agents", force: :cascade do |t|
    t.string   "username"
    t.string   "password"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "tehsil_id"
    t.date     "last_active"
    t.string   "eamil"
  end

  create_table "child_health_screenings", force: :cascade do |t|
    t.integer  "student_id"
    t.integer  "bmi"
    t.boolean  "anemia"
    t.boolean  "physical_deformities"
    t.boolean  "hearing_problem"
    t.boolean  "ear_problem"
    t.boolean  "vision"
    t.boolean  "eye_problem"
    t.boolean  "respiratory_infection"
    t.boolean  "skin_problem"
    t.boolean  "trauma_physical_injuries"
    t.string   "comments"
    t.boolean  "referred"
    t.date     "follow_up_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "agent_id"
    t.string   "owner_type"
    t.integer  "owner_id"
    t.boolean  "jaundice"
    t.boolean  "facial_puffiness"
    t.boolean  "edema"
    t.boolean  "mental_dissability"
    t.boolean  "ho_immunisation"
    t.boolean  "ho_allergy"
    t.boolean  "ho_hospitalisation"
    t.boolean  "dental_problem"
    t.date     "parent_counciling_date"
    t.boolean  "parent_counciling_required"
    t.boolean  "student_referred"
    t.boolean  "body_nits"
    t.boolean  "lice_in_hair"
    t.boolean  "is_follow_up"
  end

  create_table "council_meetings", force: :cascade do |t|
    t.date     "date_of_meeting"
    t.boolean  "school_env"
    t.boolean  "school_canteen"
    t.boolean  "epidamic_disease_avr"
    t.boolean  "hygiene"
    t.boolean  "teacher_orientation_health_topics"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "agent_id"
    t.integer  "school_id"
  end

  create_table "daily_plans", force: :cascade do |t|
    t.integer  "agent_id"
    t.date     "due_date"
    t.date     "completed_date"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "name"
    t.integer  "plan_type"
  end

  create_table "districts", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "division_id"
    t.integer  "province_id"
  end

  create_table "divisions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "environment_assesments", force: :cascade do |t|
    t.boolean  "boundy_wall_gate"
    t.boolean  "electricity"
    t.boolean  "playground"
    t.boolean  "clean_class_toilet"
    t.boolean  "clean_floors"
    t.boolean  "janitors"
    t.boolean  "ventilation"
    t.boolean  "tables_chairs"
    t.boolean  "fans"
    t.boolean  "fountains"
    t.boolean  "clean_water"
    t.boolean  "supply"
    t.boolean  "functional_toilets"
    t.boolean  "toilet_numbers"
    t.boolean  "soap_water_toilets"
    t.boolean  "drainage_functioning"
    t.boolean  "waste_managment"
    t.integer  "waste_managemnt_freq"
    t.string   "comments"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "school_id"
    t.integer  "agent_id"
    t.integer  "condition_of_building"
    t.integer  "cleanliness_of_school_environment"
    t.integer  "class_room_space_adequacy"
    t.integer  "availability_of_safe_drinking_water"
    t.integer  "sanitary_condition_of_toilets"
    t.integer  "drainage_condiiton"
    t.integer  "waste_management"
    t.integer  "cantain_and_food_quality"
    t.boolean  "roof_status"
    t.boolean  "running_water"
    t.boolean  "drainage_system"
    t.integer  "food_quality"
    t.string   "support_person_designtion"
    t.string   "support_person_name"
    t.boolean  "lights_present"
    t.boolean  "soap_container"
    t.boolean  "physical_activity_provisions"
    t.boolean  "stagnent_water"
    t.boolean  "stagnent_water_removal_plan"
    t.boolean  "dates"
    t.boolean  "boiled_sweet_potato"
    t.boolean  "boiled_potato"
    t.boolean  "boiled_roasted_corn"
    t.boolean  "home_cooked_meals"
    t.boolean  "clean_canteen"
    t.boolean  "standards_displayed"
    t.boolean  "fitness_food_handlers"
  end

  create_table "follow_ups", force: :cascade do |t|
    t.string   "owner_type"
    t.integer  "owner_id"
    t.date     "schedule_date"
    t.boolean  "completed"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "agent_id"
    t.decimal  "lat"
    t.decimal  "long"
    t.date     "completed_date"
  end

  create_table "health_education_sessions", force: :cascade do |t|
    t.integer  "felmale_no"
    t.string   "topic"
    t.date     "session_date"
    t.integer  "agent_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "owner_type"
    t.integer  "owner_id"
    t.string   "advisor"
    t.decimal  "lat"
    t.decimal  "long"
    t.integer  "no_of_participant"
    t.integer  "no_of_teacher"
    t.string   "advisor_designation"
  end

  create_table "location_histories", force: :cascade do |t|
    t.decimal  "lat"
    t.decimal  "long"
    t.integer  "agent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "parent_counciling_sessions", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "agent_id"
    t.integer  "student_id"
    t.string   "opt1"
    t.string   "opt2"
  end

  create_table "provinces", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], name: "index_roles_on_name"

  create_table "school_health_meetings", force: :cascade do |t|
    t.integer  "school_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "agent_id"
  end

  create_table "schools", force: :cascade do |t|
    t.decimal  "lat"
    t.decimal  "long"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "tehsil_id"
    t.date     "last_visited"
    t.integer  "uc_id"
    t.integer  "district_id"
    t.string   "school_level"
    t.integer  "school_gender"
    t.integer  "class_rooms"
    t.string   "dist_name"
    t.string   "teh_name"
    t.string   "unc_name"
    t.integer  "emis"
  end

  create_table "students", force: :cascade do |t|
    t.integer  "grade"
    t.string   "name"
    t.string   "gaurdian_name"
    t.string   "father_name"
    t.string   "dob"
    t.integer  "siblings"
    t.decimal  "hieght"
    t.decimal  "weight"
    t.integer  "school_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "agent_id"
    t.date     "last_assessed"
    t.integer  "gender"
  end

  create_table "tehsils", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "district_id"
  end

  create_table "union_concils", force: :cascade do |t|
    t.string   "name"
    t.integer  "tehsil_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
