class AddLiceInHairToStudentHealthAssesment < ActiveRecord::Migration
  def change
    add_column :child_health_screenings, :lice_in_hair, :boolean
  end
end
