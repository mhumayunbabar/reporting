class AddOwnerTypeToChildHealthScreening < ActiveRecord::Migration
  def change
    add_column :child_health_screenings, :owner_type, :string
  end
end
