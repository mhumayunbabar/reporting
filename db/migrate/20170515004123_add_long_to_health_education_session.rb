class AddLongToHealthEducationSession < ActiveRecord::Migration
  def change
    add_column :health_education_sessions, :long, :decimal
  end
end
