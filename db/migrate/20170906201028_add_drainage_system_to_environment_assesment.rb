class AddDrainageSystemToEnvironmentAssesment < ActiveRecord::Migration
  def change
    add_column :environment_assesments, :drainage_system, :boolean
  end
end
