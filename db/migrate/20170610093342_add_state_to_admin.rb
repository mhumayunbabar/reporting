class AddStateToAdmin < ActiveRecord::Migration
  def change
    add_column :admins, :state, :integer, default: 1
  end
end
