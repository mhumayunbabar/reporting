class AddCleanlinessOfSchollEnvironmentToEnvironmentAssesment < ActiveRecord::Migration
  def change
    add_column :environment_assesments, :cleanliness_of_school_environment, :integer
  end
end
