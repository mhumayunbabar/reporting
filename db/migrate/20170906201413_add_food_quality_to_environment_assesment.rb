class AddFoodQualityToEnvironmentAssesment < ActiveRecord::Migration
  def change
    add_column :environment_assesments, :food_quality, :integer
  end
end
