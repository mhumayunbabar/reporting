class AddAvailabilityOfSafeDrinkingWaterToEnvironmentAssesment < ActiveRecord::Migration
  def change
    add_column :environment_assesments, :availability_of_safe_drinking_water, :integer
  end
end
