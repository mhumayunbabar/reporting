class AddEdemaToChildHealthScreening < ActiveRecord::Migration
  def change
    add_column :child_health_screenings, :edema, :boolean
  end
end
