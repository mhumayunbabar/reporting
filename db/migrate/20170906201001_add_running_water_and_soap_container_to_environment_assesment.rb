class AddRunningWaterAndSoapContainerToEnvironmentAssesment < ActiveRecord::Migration
  def change
    add_column :environment_assesments, :running_water_and_soap_container, :boolean
  end
end
