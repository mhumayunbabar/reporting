class AddSweetBoiledPotatoToEnvironmentAssesment < ActiveRecord::Migration
  def change
    add_column :environment_assesments, :boiled_sweet_potato, :boolean
  end
end
