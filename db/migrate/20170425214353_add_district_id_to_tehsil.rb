class AddDistrictIdToTehsil < ActiveRecord::Migration
  def change
    add_column :tehsils, :district_id, :integer
  end
end
