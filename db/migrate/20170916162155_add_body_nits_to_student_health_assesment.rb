class AddBodyNitsToStudentHealthAssesment < ActiveRecord::Migration
  def change
    add_column :child_health_screenings, :body_nits, :boolean
  end
end
