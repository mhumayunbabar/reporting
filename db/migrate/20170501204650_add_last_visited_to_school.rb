class AddLastVisitedToSchool < ActiveRecord::Migration
  def change
    add_column :schools, :last_visited, :date
  end
end
