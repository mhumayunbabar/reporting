class CreateParentCouncilingSessions < ActiveRecord::Migration
  def change
    create_table :parent_counciling_sessions do |t|
      t.integer :school_id

      t.timestamps null: false
    end
  end
end
