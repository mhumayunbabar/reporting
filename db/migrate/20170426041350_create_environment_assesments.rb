class CreateEnvironmentAssesments < ActiveRecord::Migration
  def change
    create_table :environment_assesments do |t|
      t.boolean :boundy_wall_gate
      t.boolean :electricity
      t.boolean :playground
      t.boolean :clean_class_toilet
      t.boolean :clean_floors
      t.boolean :janitors
      t.boolean :ventilation
      t.boolean :tables_chairs
      t.boolean :fans
      t.boolean :fountains
      t.boolean :clean_water
      t.boolean :supply
      t.boolean :functional_toilets
      t.boolean :toilet_numbers
      t.boolean :soap_water_toilets
      t.boolean :drainage
      t.boolean :drainage_functioning
      t.boolean :waste_managment
      t.integer :waste_managemnt_freq
      t.string :comments

      t.timestamps
    end
  end
end
