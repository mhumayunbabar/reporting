class AddOwnerIdToChildHealthScreening < ActiveRecord::Migration
  def change
    add_column :child_health_screenings, :owner_id, :integer
  end
end
