class CreateSchools < ActiveRecord::Migration
  def change
    create_table :schools do |t|
      t.decimal :lat
      t.decimal :long
      t.string :name
      t.integer :district_id

      t.timestamps
    end
  end
end
