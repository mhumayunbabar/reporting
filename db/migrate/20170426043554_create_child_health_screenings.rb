class CreateChildHealthScreenings < ActiveRecord::Migration
  def change
    create_table :child_health_screenings do |t|
      t.integer :student_id
      t.integer :bmi
      t.boolean :anemia
      t.boolean :physical_deformities
      t.boolean :hearing_problem
      t.boolean :ear_problem
      t.boolean :vision
      t.boolean :eye_problem
      t.boolean :respiratory_infection
      t.boolean :skin_problem
      t.boolean :trauma_physical_injuries
      t.string :comments
      t.boolean :referred
      t.date :follow_up_date

      t.timestamps
    end
  end
end
