class AddAdvisorDesignationToHealthEducationSession < ActiveRecord::Migration
  def change
    add_column :health_education_sessions, :advisor_designation, :string
  end
end
