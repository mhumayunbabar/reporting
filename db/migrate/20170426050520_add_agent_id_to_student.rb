class AddAgentIdToStudent < ActiveRecord::Migration
  def change
    add_column :students, :agent_id, :integer
  end
end
