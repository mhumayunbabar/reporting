class AddParentCouncilingDateToChildHealthScreening < ActiveRecord::Migration
  def change
    add_column :child_health_screenings, :parent_counciling_date, :date
  end
end
