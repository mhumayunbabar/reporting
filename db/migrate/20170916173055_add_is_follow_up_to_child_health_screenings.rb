class AddIsFollowUpToChildHealthScreenings < ActiveRecord::Migration
  def change
    add_column :child_health_screenings, :is_follow_up, :boolean
  end
end
