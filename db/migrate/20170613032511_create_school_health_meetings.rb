class CreateSchoolHealthMeetings < ActiveRecord::Migration
  def change
    create_table :school_health_meetings do |t|
      t.integer :school_id

      t.timestamps null: false
    end
  end
end
