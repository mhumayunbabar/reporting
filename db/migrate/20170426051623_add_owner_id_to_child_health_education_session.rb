class AddOwnerIdToChildHealthEducationSession < ActiveRecord::Migration
  def change
    add_column :health_education_sessions, :owner_id, :integer
  end
end
