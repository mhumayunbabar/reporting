class AddStandardDisplayedToEnvironmentAssesment < ActiveRecord::Migration
  def change
    add_column :environment_assesments, :standards_displayed, :boolean
  end
end
