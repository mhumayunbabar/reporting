class AddSchoolIdToCouncilMeeting < ActiveRecord::Migration
  def change
    add_column :council_meetings, :school_id, :integer
  end
end
