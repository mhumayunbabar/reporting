class CreateUnionConcils < ActiveRecord::Migration
  def change
    create_table :union_concils do |t|
      t.string :name
      t.integer :tehsil_id

      t.timestamps
    end
  end
end
