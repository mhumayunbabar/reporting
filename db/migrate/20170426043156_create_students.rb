class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.integer :grade
      t.string :name
      t.string :gaurdian_name
      t.string :father_name
      t.string :dob
      t.integer :siblings
      t.decimal :hieght
      t.decimal :weight
      t.integer :school_id

      t.timestamps
    end
  end
end
