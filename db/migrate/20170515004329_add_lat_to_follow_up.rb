class AddLatToFollowUp < ActiveRecord::Migration
  def change
    add_column :follow_ups, :lat, :decimal
  end
end
