class AddClassRoomSpaceAdequacyToEnvironmentAssesment < ActiveRecord::Migration
  def change
    add_column :environment_assesments, :class_room_space_adequacy, :integer
  end
end
