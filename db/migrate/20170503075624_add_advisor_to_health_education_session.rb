class AddAdvisorToHealthEducationSession < ActiveRecord::Migration
  def change
    add_column :health_education_sessions, :advisor, :string
  end
end
