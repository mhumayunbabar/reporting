class AddParentCouncilingRequiredToChildHealthScreening < ActiveRecord::Migration
  def change
    add_column :child_health_screenings, :parent_counciling_required, :boolean
  end
end
