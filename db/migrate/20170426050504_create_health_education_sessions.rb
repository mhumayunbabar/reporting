class CreateHealthEducationSessions < ActiveRecord::Migration
  def change
    create_table :health_education_sessions do |t|
      t.integer :male_no
      t.integer :felmale_no
      t.string :topic
      t.date :session_date
      t.integer :agent_id

      t.timestamps
    end
  end
end
