class AddHomeCookedMealsToEnvironmentAssesment < ActiveRecord::Migration
  def change
    add_column :environment_assesments, :home_cooked_meals, :boolean
  end
end
