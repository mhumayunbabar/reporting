class AddDentalProblemToChildHealthScreening < ActiveRecord::Migration
  def change
    add_column :child_health_screenings, :dental_problem, :boolean
  end
end
