class AddAgentIdToSchoolHealthMeeting < ActiveRecord::Migration
  def change
    add_column :school_health_meetings, :agent_id, :integer
  end
end
