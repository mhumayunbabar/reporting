class CreateDailyPlans < ActiveRecord::Migration
  def change
    create_table :daily_plans do |t|
      t.integer :agent_id
      t.date :due_date
      t.date :completed_date

      t.timestamps null: false
    end
  end
end
