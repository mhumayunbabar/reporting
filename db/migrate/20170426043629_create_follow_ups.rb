class CreateFollowUps < ActiveRecord::Migration
  def change
    create_table :follow_ups do |t|
      t.string :owner_type
      t.integer :owner_id
      t.date :schedule_date
      t.boolean :completed

      t.timestamps
    end
  end
end
