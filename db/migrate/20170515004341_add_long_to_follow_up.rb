class AddLongToFollowUp < ActiveRecord::Migration
  def change
    add_column :follow_ups, :long, :decimal
  end
end
