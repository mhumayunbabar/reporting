class AddFitnessFoodHandlersToEnvironmentAssesment < ActiveRecord::Migration
  def change
    add_column :environment_assesments, :fitness_food_handlers, :boolean
  end
end
