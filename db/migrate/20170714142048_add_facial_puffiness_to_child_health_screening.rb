class AddFacialPuffinessToChildHealthScreening < ActiveRecord::Migration
  def change
    add_column :child_health_screenings, :facial_puffiness, :boolean
  end
end
