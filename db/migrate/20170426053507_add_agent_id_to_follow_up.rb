class AddAgentIdToFollowUp < ActiveRecord::Migration
  def change
    add_column :follow_ups, :agent_id, :integer
  end
end
