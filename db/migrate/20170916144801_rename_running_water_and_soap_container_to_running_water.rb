class RenameRunningWaterAndSoapContainerToRunningWater < ActiveRecord::Migration
  def change
  	rename_column :environment_assesments, :running_water_and_soap_container, :running_water 
  end
end
