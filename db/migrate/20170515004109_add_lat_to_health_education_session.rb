class AddLatToHealthEducationSession < ActiveRecord::Migration
  def change
    add_column :health_education_sessions, :lat, :decimal
  end
end
