class CreateCouncilMeetings < ActiveRecord::Migration
  def change
    create_table :council_meetings do |t|
      t.date :date_of_meeting
      t.boolean :school_env
      t.boolean :school_canteen
      t.boolean :epidamic_disease_avr
      t.boolean :hygiene
      t.boolean :teacher_orientation_health_topics

      t.timestamps null: false
    end
  end
end
