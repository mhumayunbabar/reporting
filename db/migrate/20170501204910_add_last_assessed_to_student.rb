class AddLastAssessedToStudent < ActiveRecord::Migration
  def change
    add_column :students, :last_assessed, :date
  end
end
