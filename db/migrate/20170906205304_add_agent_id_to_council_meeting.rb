class AddAgentIdToCouncilMeeting < ActiveRecord::Migration
  def change
    add_column :council_meetings, :agent_id, :integer
  end
end
