class AddStudentReferredToChildHealthScreening < ActiveRecord::Migration
  def change
    add_column :child_health_screenings, :student_referred, :boolean
  end
end
