class AddMentalDissiabilityToChildHealthScreening < ActiveRecord::Migration
  def change
    add_column :child_health_screenings, :mental_dissability, :boolean
  end
end
