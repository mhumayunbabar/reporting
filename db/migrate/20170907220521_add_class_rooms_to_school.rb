class AddClassRoomsToSchool < ActiveRecord::Migration
  def change
    add_column :schools, :class_rooms, :integer
  end
end
