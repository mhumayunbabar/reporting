class AddCompletedDateToFollowUp < ActiveRecord::Migration
  def change
    add_column :follow_ups, :completed_date, :date
  end
end
