class AddSchoolGenderToSchool < ActiveRecord::Migration
  def change
    add_column :schools, :school_gender, :integer
  end
end
