class AddNameToDailyPlans < ActiveRecord::Migration
  def change
    add_column :daily_plans, :name, :string
  end
end
