class CreateLocationHistories < ActiveRecord::Migration
  def change
    create_table :location_histories do |t|
      t.decimal :lat
      t.decimal :long
      t.integer :agent_id

      t.timestamps null: false
    end
  end
end
