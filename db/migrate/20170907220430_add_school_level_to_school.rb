class AddSchoolLevelToSchool < ActiveRecord::Migration
  def change
    add_column :schools, :school_level, :string
  end
end
