class AddOwnerTypeToHealthEducationSesison < ActiveRecord::Migration
  def change
    add_column :health_education_sessions, :owner_type, :string
  end
end
