class AddDistrictIdToAdmin < ActiveRecord::Migration
  def change
    add_column :admins, :district_id, :integer
  end
end
