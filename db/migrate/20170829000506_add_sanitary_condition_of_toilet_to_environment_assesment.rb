class AddSanitaryConditionOfToiletToEnvironmentAssesment < ActiveRecord::Migration
  def change
    add_column :environment_assesments, :sanitary_condition_of_toilets, :integer
  end
end
