class AddAgentIdToChildHealthScreening < ActiveRecord::Migration
  def change
    add_column :child_health_screenings, :agent_id, :integer
  end
end
