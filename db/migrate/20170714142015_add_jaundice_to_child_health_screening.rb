class AddJaundiceToChildHealthScreening < ActiveRecord::Migration
  def change
    add_column :child_health_screenings, :jaundice, :boolean
  end
end
