class AddLastActiveToAgent < ActiveRecord::Migration
  def change
    add_column :agents, :last_active, :date
  end
end
