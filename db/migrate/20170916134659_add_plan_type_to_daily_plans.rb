class AddPlanTypeToDailyPlans < ActiveRecord::Migration
  def change
    add_column :daily_plans, :plan_type, :integer
  end
end
