namespace :one_time_rake do
  desc "Seed data"

  task import_school_data: :environment do
     require 'csv'    

        csv_text = File.read('./schools.csv')
        csv = CSV.parse(csv_text)
        # ["emis", "name", "district_name", "tehsil_name", "uc_name", "school_level", "school_gender", "class_rooms", "lat", "long"]
        csv.each do |row|
            district = District.find_or_create_by(name: row[2])
            tehsil = Tehsil.find_or_create_by(name: row[3])
            tehsil.update(district_id: district.id) unless tehsil.district_id.present?
            uc = UnionConcil.find_or_create_by(name: row[4])
            uc.update(tehsil_id: tehsil.id) unless uc.tehsil_id.present?
          x = School.new(tehsil_id: tehsil.id, district_id: district.id, uc_id: uc.id ,emis: row[0], name: row[1], dist_name: row[2], teh_name: row[3], unc_name: row[4], school_level: row[5], school_gender: row[6] == "MALE" ? 0 : 1 ,class_rooms: row[7], lat: row[8], long: row[9])
          x.save!
        end

   end

  task seed_dummy_data: :environment do
    puts("cleaning db...")
    # [
    #   Division, District, Tehsil, UnionConcil, School, EnvironmentAssesment
    # ].each do |m|
    #   ActiveRecord::Base.connection.execute("TRUNCATE TABLE #{m.table_name} RESTART IDENTITY;")
    # end
    Admin.create_dummy
    puts("reseeding db...")

    1.upto(10).each do |number| 
       Division.create(name: "Division" + Random.rand(10).to_s)
    end

    1.upto(10).each do |number| 
       Agent.create(username: "SHNS" + Random.rand(10).to_s, password: Digest::MD5.hexdigest("SHNS"),first_name: "FirstName", last_name: "last_name", tehsil_id: 1 + Random.rand(10) )
    end

    1.upto(10).each do |number| 
      District.create(name: "District" + Random.rand(10).to_s, division_id: 1+ Random.rand(10))
    end

    1.upto(10).each do |number| 
      Tehsil.create(name: "Tehsil" + Random.rand(10).to_s, district_id: 1+ Random.rand(10))
    end

    1.upto(10).each do |number| 
      UnionConcil.create(name: "UC" + Random.rand(10).to_s, tehsil_id: 1+ Random.rand(10))
    end

    1.upto(10).each do |number| 
      School.create(lat: 31.5546, long: 74.3572, tehsil_id: 1+ Random.rand(10), last_visited: Random.rand(10).days.from_now , name: "School" + Random.rand(100).to_s)    
    end

    1.upto(10).each do |number|
        Student.create(school_id: 1 + Random.rand(10), grade: 1+ Random.rand(8),
            name: "Name" + Random.rand(10).to_s, 
            gender: Random.rand(2),
            gaurdian_name: "Gaurdian" + Random.rand(10).to_s,
            father_name: "Father" + Random.rand(10).to_s,
            siblings: Random.rand(4),
            last_assessed: Random.rand(10).days.ago )
    end

    1.upto(10).each do |number| 
      EnvironmentAssesment.create(
        boundy_wall_gate: Random.rand(2),
        electricity: Random.rand(2),
        playground: Random.rand(2),
        clean_class_toilet: Random.rand(2),
        clean_floors: Random.rand(2),
        janitors: Random.rand(2),
        ventilation: Random.rand(2),
        tables_chairs: Random.rand(2),
        fans: Random.rand(2),
        fountains: Random.rand(2),
        clean_water: Random.rand(2),
        supply: Random.rand(2),
        functional_toilets: Random.rand(2),
        toilet_numbers: Random.rand(2),
        soap_water_toilets: Random.rand(2),
        drainage_functioning: Random.rand(2),
        waste_managment: Random.rand(2),
        waste_managemnt_freq: Random.rand(3),
        comments: "Dummy comments",
        school_id: 1+ Random.rand(10),
        agent_id: 1+ Random.rand(10),
        created_at: Random.rand(10).days.from_now ,
        condition_of_building: 1+ Random.rand(3),
        cleanliness_of_school_environment: 1+ Random.rand(3),
        class_room_space_adequacy: 1+ Random.rand(3),
        availability_of_safe_drinking_water: 1+ Random.rand(3),
        sanitary_condition_of_toilets: 1+ Random.rand(3),
        drainage_condiiton: 1+ Random.rand(3),
        waste_management: 1+ Random.rand(3),
                roof_status: Random.rand(2),
        drainage_system: Random.rand(2),
        food_quality: Random.rand(3),
        support_person_designtion: "Poistion",
        support_person_name: "Name",
        cantain_and_food_quality: 1+ Random.rand(3)
        ) 
    end

    1.upto(10).each do |number| 
      ChildHealthScreening.create(
        owner_type: "Student",
        bmi: 1+ Random.rand(40),
        anemia: Random.rand(2),
        physical_deformities: Random.rand(2),
        hearing_problem: Random.rand(2),
        ear_problem: Random.rand(2),
        vision: Random.rand(2),
        eye_problem: Random.rand(2),
        respiratory_infection: Random.rand(2),
        skin_problem: Random.rand(2),
        trauma_physical_injuries: Random.rand(2),
        owner_id: 1 + Random.rand(10),
        student_id: 1+ Random.rand(10),
        agent_id: 1+ Random.rand(10),
        created_at: Random.rand(10).days.from_now 
        ) 
    end

    1.upto(10).each do |number| 
      SchoolHealthMeeting.create(
        agent_id: 1 + Random.rand(10),
        school_id: 1+ Random.rand(10),
        created_at: Random.rand(10).days.from_now 
        ) 
    end

     1.upto(10).each do |number| 
        check =  Random.rand(10) % 2 == 0
      FollowUp.create(
        agent_id: 1 + Random.rand(10),
        owner_type: "Student",
        owner_id: 1+ Random.rand(10),
        schedule_date: (1+ Random.rand(10)).days.ago,
        completed_date: check ? (1+ Random.rand(10)).days.ago : nil,
        completed: check,
        created_at: Random.rand(10).days.from_now 
        ) 
    end

    1.upto(10).each do |number|
        HealthEducationSession.create(
                        no_of_participant: Random.rand(10) + 1,
                        no_of_teacher: Random.rand(10) + 1,
            topic: "Topic" + Random.rand(10).to_s,
            session_date: (1+Random.rand(10)).days.ago,
            agent_id: Random.rand(10) + 1,
            owner_type: Random.rand(10) % 2 == 0 ? "Tehsil" : "School" ,
            owner_id: Random.rand(10) + 1,
            advisor: "Name" )
    end

    1.upto(10).each do |number| 
      DailyPlan.create(
        agent_id: 1 + Random.rand(10),
        due_date: (1 + Random.rand(10)).days.ago,
        completed_date: Random.rand(10) % 2 == 0 ? (1+ Random.rand(10)).days.ago : nil,
        created_at: Random.rand(10).days.from_now 
        ) 
    end



  end

end

# bundle exec rake one_time_rake:seed_dummy_data 