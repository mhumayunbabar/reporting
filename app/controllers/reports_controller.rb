class ReportsController < ApplicationController
  before_action :authenticate_admin, except: [:login,:session_login]
  before_action :load_admin, except: [:login, :session_login]
	
  def students
      @links_list = {
        "Dahsboard" => admin_dashboard_path,
        "Export Data" => admin_export_data_path,
       }
      respond_to do |format|
        format.xlsx {

        package = Axlsx::Package.new
            package.workbook do |wb|
                wb.add_worksheet do |sheet|
                row = params[:cols].split(',') + params[:cols_health].split(',')
                row =  row.map {|c| FRIENDLY_NAMES_REPORTS[c]}
                sheet.add_row row
                Student.create_excel_sheet params, sheet
            end
           end
         send_data package.to_stream.read, :filename => 'Students.xlsx', :type => "application/vnd.openxmlformates-officedocument.spreadsheetml.sheet"
       }

       format.html {

          @sheet = Student.create_excel params

       }
      end
    end

    def daily_community_visits
      
   
    end

   def daily_school_visits
     @links_list = {
        "Dahsboard" => admin_dashboard_path,
        "Export Reports" => admin_daily_school_visits_path,
    }
      @schools_visited = School.filter_by_range_visited params
      @environment_assessments = EnvironmentAssesment.filter params
      @heatlhs_screenings = ChildHealthScreening.filter(params).joins(:student)
      @follow_ups_today = FollowUp.filter(params)
      @children_screened_today = Student.filter_by_range_assesed params
      @uc_tehsil = Tehsil.where(id: params[:tehsil]) if params[:tehsil].present?
      @school = School.find_by_id(params[:school]) if params[:school].present?
      respond_to do |format|
      
       format.html {

          

       }
      end
    end


    def monthly_school_visits

	    respond_to do |format|
      
       format.html {

          

       }
    	end
  	end



  def schools
 @links_list = {
        "Dahsboard" => admin_dashboard_path,
        "Export Data" => admin_export_data_path,
    }
      respond_to do |format|
        format.xlsx {
        #       send_data Student.to_xlsx.to_stream.read, :filename => 'Students.xlsx', :type => "application/vnd.openxmlformates-officedocument.spreadsheetml.sheet"
        #  }  


        package = Axlsx::Package.new
            package.workbook do |wb|
                wb.add_worksheet do |sheet|
                row = params[:cols].split(',') + params[:cols_health].split(',') + params[:cols_env].split(',')
                row =  row.map {|c| FRIENDLY_NAMES_REPORTS[c]}
                sheet.add_row row

                School.create_excel_sheet params, sheet
            end
           end
         send_data package.to_stream.read, :filename => 'Schools.xlsx', :type => "application/vnd.openxmlformates-officedocument.spreadsheetml.sheet"
       }
       format.html {

          @sheet = School.create_excel params

       }
      end
    end

  	def health_sessions
  	    respond_to do |format|
        format.xlsx {
              send_data School.to_xlsx.to_stream.read, :filename => 'Schools.xlsx', :type => "application/vnd.openxmlformates-officedocument.spreadsheetml.sheet"
         }  
    	end
  	
  	end


	def environment_assessments
		respond_to do |format|
        format.xlsx {
              send_data School.to_xlsx.to_stream.read, :filename => 'Schools.xlsx', :type => "application/vnd.openxmlformates-officedocument.spreadsheetml.sheet"
         }  
    	end
	end

   def authenticate_admin
   if session[:admin].blank? || session[:new_check].blank?
      reset_session
      redirect_to({ :action=>'login' }, notice: 'Please login to conitnue.') 
   end
  end 

  def load_admin
    @links_list = {}
    @admin = Admin.find_by_id(session[:admin])
  end 

end
