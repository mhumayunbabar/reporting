class ApiController < ApplicationController
	before_action :load_agent, except: [:login]
	skip_before_filter :verify_authenticity_token

	after_action :agent_activity, only: [:submit_environment_assesment, :submit_child_health_screening, :submit_council_meeting, :submit_add_child]
	def login
		@agent = Agent.login(params[:u_name], params[:password])
		if @agent.present?
			todays_followups = @agent.get_followups(Date.today)
		end
		result = {
			authentication: @agent.present?,
			agent_id: @agent.try(:id),
			follow_ups: todays_followups || "No Follow Ups",
			name: @agent.try(:name) || "" ,
			message: @agent.present? ? "Login Successful!" : "Username or Password wrong!"
		}
		respond_to do |format|
           format.json do
           render json: result
          end
        end
	end

	def get_nearby_schools
		@schools = School.by_tehsil(@agent.tehsil_id)     
		@filtered_schools = @schools.select {|school| haversine(school.lat, school.long, params[:lat], params[:long]) < NEAR_DISTANCE }
		result = {
			schools: @filtered_schools.map {|school| {id: school.emis, name: school.name }}
		}
		respond_to do |format|
           format.json do
           render json: result
          end
        end
	end

	def search_schools
		@schools = School.by_tehsil(@agent.tehsil_id)     
		@filtered_schools = @schools.search(params[:query]).result(distinct: true)

		result = {
			schools: @filtered_schools.map {|school| {id: school.emis, name: school.name }}
		}
		respond_to do |format|
           format.json do
           render json: result
          end
        end
	end

	def search_schools_for_select
		binding.pry
		parameters = params[:query]
		@filtered_schools = School.where("`name` like '%#{parameters[:term]}%'")

		result = @filtered_schools.limit(10).map {|school| {id: school.id, name: school.name }}
		respond_to do |format|
           format.json do
           render json: result
          end
        end
	end
	
	def search_districts_for_select
		parameters = params[:query]
		@filtered_district = District.where("`name` like '%#{parameters[:term]}%'")

		result = @filtered_district.limit(10).map {|school| {id: school.id, name: school.name }}
		respond_to do |format|
           format.json do
           render json: result
          end
        end
	end

	def search_tehsil_for_select
		parameters = params[:query]
		@filtered_tehsils = Tehsil.where("`name` like '%#{parameters[:term]}%'")

		result = @filtered_tehsils.limit(10).map {|school| {id: school.id, name: school.name }}
		respond_to do |format|
           format.json do
           render json: result
          end
        end
	end

	def get_ucs
		@union_concils = UnionConcil.by_tehsil(@agent.tehsil_id)     
		result = {
			ucs: @union_concils.map {|uc| {id: uc.id, name: uc.name }}
		}
		respond_to do |format|
           format.json do
           render json: result
          end
        end

	end
	
	def search_uc
		@union_concils = UnionConcil.by_tehsil(@agent.tehsil_id)     
		@filtered_union_concils = @union_concils.search(params[:query]).result(distinct: true)

		result = {
			ucs: @filtered_union_concils.map {|uc| {id: uc.id, name: uc.name }}
		}
		respond_to do |format|
           format.json do
           render json: result
          end
        end
	end
	

	def create_student
		student_params = params[:student].split(';')	
		@student = Student.create(grade: student_params[0], gaurdian_name: student_params[1], father_name: student_params[2],
		dob: student_params[3], last_assessed: Date.today, agent_id: params[:agent])
		respond_to do |format|
			format.json do
				render json: {studnet_id: @student.id}
			end
		end

	end
	# dental_problem to be added
	# parent_counciling_date to be added (date)
	# parent_counciling_required to be added
	# student_referred to be added
	def submit_child_health_screening
		parameters = params[:data].split(';')
		student_params = params[:student].split(';')
	
		@student = Student.find_by_id(params[:child_id])

		@student.update(siblings: student_params[0], hieght: student_params[2], weight: student_params[1])
		school = School.find_by_emis(params[:place_id])
		@student.update(school_id: school.id)
		school.update(last_visited: Date.today)
		

		@child_health_screening = ChildHealthScreening.create(student_id: @student.id, bmi: parameters[0], anemia: parameters[1] == 1 , jaundice: parameters[2] == 1,
facial_puffiness: parameters[3] == 1, body_nits: parameters[4] === 1,	physical_deformities: parameters[5] == 1, 	mental_dissability: parameters[6] == 1,  hearing_problem: parameters[7] == 1 , ear_problem: parameters[8] == 1, 
			vision: parameters[9] == 1 , eye_problem: parameters[10] == 1, dental_problem: parameters[11], respiratory_infection: parameters[12] == 1,
			skin_problem: parameters[13] == 1, trauma_physical_injuries: parameters[14] == 1,  ho_immunisation: parameters[15],
			ho_allergy: parameters[16],
ho_hospitalisation: parameters[17], parent_counciling_required: parameters[18] == 1,student_referred: parameters[19], lice_in_hair: parameters[20] == 1,  comments: params[:other_problem], agent_id: params[:agent],
			owner_id: school.id, owner_type: "School", parent_counciling_date: params[:pcdate], is_follow_up: params[:is_follow_up] == 1 )

			if params[:fdate].present?
				@child_health_screening.update(follow_up_date: params[:fdate].to_date)
				@follow_up = FollowUp.create(owner_type: "Student", owner_id: params[:student_id], lat: params[:lat], long: params[:long])
			end
			if params[:is_follow_up] == 1
				@student.follow_ups.last.udpate(completed: true, completed_date: Date.today)
			end

		result = {
			message: @child_health_screening.present? ? "Child Health ScreeningAdded Successfully" : "Something went wrong",
			success: @child_health_screening.present?
		}

		respond_to do |format|
           format.json do
           render json: result 
          end
        end

	end

	def submit_council_meeting
		parameters = params[:data].split(';')
		school = School.find_by_emis(params[:place_id])
		@council = CouncilMeeting.create(date_of_meeting: parameters[5],
						school_id: school.id,
						school_env: parameters[0],
						school_canteen: parameters[1],
						epidamic_disease_avr: parameters[2],
						hygiene: parameters[3],
						teacher_orientation_health_topics: parameters[4],agent_id: params[:agent])

		result = {
			message: @council.present? ? "Council Meeting information Added Successfully" : "Something went wrong",
			success: @council.present?
		}

		respond_to do |format|
           format.json do
           render json: result 
          end
        end
	end

		
	def submit_health_session
		#fix this shit, for the place_id emis
		school = School.find_by_emis(params[:place_id])
		owner_id = school.id if school.present?
		@health = HealthEducationSession.create(no_of_participant: params[:noofParticipant], advisor_designation: params[:advisor_desg] ,no_of_teacher: params[:noofTeacher], topic: params[:topic_discussed],
			session_date: params[:date].to_date, agent_id: params[:agent], owner_type: params[:owner_type], owner_id: owner_id, advisor: params[:advisor], lat: params[:lat], long: params[:long])


		if params[:owner_type] == 'School'
			School.find_by_emis(params[:owner_id]).update(last_visited: Date.today)
		end 

		result = {
			message: @health.present? ? "Health Educaiton Session Added Successfully" : "Something went wrong",
			success: @health.present?
		}
		respond_to do |format|
           format.json do
           render json: result 
          end
        end
	end

# functional_toilets -> are tap_functions
#  hand liquid is soap_water 
# drainage is water flush out sytem

# roof_status to be added
# running_water_and_soap_container to be added
# drainage_system to be added
# food_quality (good, average, poor) to be added
# support_person_designtion to be added
# support_person_name to be added

# drainage frequencey is daily weekly monthly


	def submit_environment_assesment
		parameters = params[:data].split(';')
		school = School.find_by_emis(params[:place_id])
		@environment_assesment = EnvironmentAssesment.create(school_id: school.id, boundy_wall_gate: parameters[0] == 1 , 
			electricity: parameters[1] == 1, playground: parameters[2] == 1, clean_class_toilet: parameters[3] == 1, clean_floors: parameters[4] == 1, 
			janitors: parameters[5] == 1, ventilation: parameters[6] == 1,tables_chairs: parameters[7] == 1, fans: parameters[8] == 1, 
			clean_water: parameters[9] == 1, supply: parameters[10] == 1, functional_toilets: parameters[11] == 1,  
			soap_water_toilets: parameters[12] == 1, drainage_system: parameters[13] == 1, roof_status: parameters[14] == 1,toilet_numbers: parameters[15] == 1, 
			running_water: parameters[16] == 1, soap_container: parameters[17] == 1 , drainage_functioning: parameters[18] == 1, 
			waste_managment: parameters[19] == 1, 
			waste_managemnt_freq: parameters[20], food_quality: parameters[21], lights_present: parameters[22] == 1,
# to be added 
			 physical_activity_provisions: parameters[23] ==1,
			 stagnent_water: parameters[24] == 1,
			 stagnent_water_removal_plan: parameters[25] == 1,
			 dates: parameters[26] == 1,
			 boiled_sweet_potato: parameters[27] == 1,
			 boiled_potato: parameters[28] == 1,
			 boiled_roasted_corn: parameters[29] == 1,
			 home_cooked_meals: parameters[30] == 1,
			 clean_canteen: parameters[31] == 1,
			 standards_displayed: parameters[32] == 1,
			 fitness_food_handlers: parameters[33] == 1,
# end
			 support_person_name: parameters[34],support_person_designtion: parameters[35], 
			comments: parameters[36],agent_id: params[:agent])

		
		@environment_assesment.school.update(last_visited: Date.today)
		result = {
			message: @environment_assesment.present? ? "Environment Assesment Added Successfully" : "Something went wrong",
			success: @environment_assesment.present?
		}
		respond_to do |format|
           format.json do
           render json: result 
          end
        end
	end


	def submit_add_child
		school = School.find_by_emis(params[:place_id])
		@child = Student.create(grade: params[:grade],
			name: params[:name],
			gaurdian_name: params[:parent_name],
			father_name: params[:father_name],
			dob: params[:dob].to_date,
			agent_id: params[:agent],
			gender: params[:male], school_id: school.id)
		result = {
			message: @child.present? ? "Child Added Successfully" : "Something went wrong",
			success: @child.present?,
			id: @child.id 
		}
		respond_to do |format|
           format.json do
           render json: result 
          end
        end
	end

	def load_agent
		@agent = Agent.find_by_id(params[:agent])
	end


	def submit_planner_record
		@agent.daily_plans.create(due_date: params[:date],
					name: params[:name],
					plan_type: params[:type])
		result = {
			message: "Planner Record Added Added Successfully"
		}
			respond_to do |format|
           format.json do
           render json: result 
          end
        end
	end

# rename the couciling to counselling

	def submit_parent_counselling
		# parameters = { child_id, agent, opt1, opt2 , lat , long}
		@par = ParentCouncilingSession.create(student_id: params[:child_id],
				agent_id: params[:agent], opt1: params[:opt1], opt2: params[:opt2])
		result = {
			message: "Parent Counselling Record Added Successfully"
		}
			respond_to do |format|
           format.json do
           render json: result 
          end
        end
	end

	def agent_location_log
		@agent.update(last_active: Date.today)
		@agent.location_histories.create(lat: params[:lat], long: params[:long])
		result = {
			message: "Locaiton Added Successfully"
		}
		respond_to do |format|
           format.json do
           render json: result 
          end
        end
	end

	def agent_activity
		@agent.update(last_active: Date.today)
		@agent.location_histories.create(lat: params[:lat], long: params[:long])
	end
end
