class GraphController < ApplicationController
	
	def followups_by_day
	    stuff = FollowUp.where(schedule_date:[7.days.ago..0.days.ago]).group_by_day(:schedule_date).count
	    result = []
	    stuff.each do |obj, value|
	    	result.push({name: obj ,y: value})
		end
		respond_to do |format|
           format.json do
           render json: result
          end
        end	
	end

	def students_assessed_by_day
	    stuff = Student.where(last_assessed:[7.days.ago..0.days.ago]).group_by_day(:last_assessed).count
	    result = []
	    stuff.each do |obj, value|
	    	result.push({name: obj ,y: value})
		end
		respond_to do |format|
           format.json do
           render json: result
          end
        end	
	end

	def schools_visited_by_day
	    stuff = School.group_by_day(:last_visited).count
	    result = []
	    stuff.each do |obj, value|
	    	result.push({name: obj ,y: value})
		end
		respond_to do |format|
           format.json do
           render json: result
          end
        end	
	end


	def health_sessions_by_day
	    stuff = HealthEducationSession.where(session_date:[7.days.ago..0.days.ago]).group_by_day(:session_date).count
	    result = []
	    stuff.each do |obj, value|
	    	result.push({name: obj ,y: value})
		end
		respond_to do |format|
           format.json do
           render json: result
          end
        end	
	end


	def environmental_assesments_by_day
	    stuff = EnvironmentAssesment.where(created_at:[7.days.ago..0.days.ago]).group_by_day(:created_at).count
	    result = []
	    stuff.each do |obj, value|
	    	result.push({name: obj ,y: value})
		end
		respond_to do |format|
           format.json do
           render json: result
          end
        end	
	end




end
