class AdminController < ApplicationController
  before_action :authenticate_admin, except: [:login,:session_login]
  before_action :load_admin, except: [:login, :session_login]

  def login
  end

    def update_tehsils
        @tehsils = Tehsil.filter_by_districts params[:district_id]
        render json: @tehsils
    end 

    def update_schools
        @schools = School.by_tehsil params[:tehsil_id]
        render json: @schools
    end 

    def update_shns
        @shns = Agent.select(:id,:username).filter_by_tehsil params[:tehsil_id]
        render json: @shns
    end 

    def update_districts
        @districts = District.filter_by_divisions(params[:division])
        render json: @districts
    end


    def update_movement_map
       @shns = Agent.find_by_id(params[:shns])
       @latLongs = @shns.location_histories.filter_by_range(params).select(:lat, :long).map { |e| {lat: e.lat.to_f, lng: e.long.to_f}  }
        render json: @latLongs
    end 
   
   def update_planning_report
       @shns = Agent.find_by_id(params[:shns])
       start = params[:range].split(' - ')[0]
       date = DateTime.strptime(start, "%m/%d/%Y")
       @plans = @shns.daily_plans.filter_by_range(params).map {|e| {day: e.due_date.strftime("%A"), plan_type: e.plan_type, name: e.name}}
        render json: {data: @plans, shns: @shns.name, week: params[:range], month: date.strftime("%m") }
    end 
   
   def update_edits_and_maps
        r = Random.rand(4);
    
        case r
        when 0
            @latLongs = [{lat: 30.3763, lng: 69.3454}, {lat: 31.3755, lng: 69.3461}]
        when 1
            @latLongs = [{lat: 30.3763, lng: 69.3000}, {lat: 31.3755, lng: 69.3481}]
        when 2
            @latLongs = [{lat: 30.3770, lng: 69.34546}, {lat: 31.3785, lng: 69.3471}]
        when 3
             @latLongs = [{lat: 30.3763, lng: 69.3000}, {lat: 31.3755, lng: 69.3481}]

        end

   
        render json: @latLongs
    end 


  def update_tiles
    @total_schools_assessed = EnvironmentAssesment.filter(params).count
    @total_child_screenings = ChildHealthScreening.filter(params).count
    @total_school_health_meetings = SchoolHealthMeeting.filter(params).count
    @total_followup_marks = FollowUp.filter(params).count
    @total_followups_not_completed_by_due_date =  FollowUp.late_followups.filter(params).count
    @total_followups_completed = FollowUp.completed.filter(params).count
    @total_parent_councelling_sessions = ParentCouncilingSession.filter(params).count
    @total_daily_plans_followed = DailyPlan.filter(params).count
    @total_daily_plans_not_followed = DailyPlan.not_followed.filter(params).count


    respond_to do |format|
      format.js do 
      end
    end
  end
    def get_charts
    case params[:type]
    when "total_schools_assesed"
        @text_day_wise_sum = "Daily Schools Assessed - Sum"
        @results_date_wise_avg = "Daily Schools Assessed - Date Wise"
        @text_day_wise_sum_avg = "Daily Schools Assessed - Average"
        @text_time_of_day_wise_sum = "Total Schools Assessed by time of day - Sum "
        @text_time_of_day_wise_avg = "Total Schools Assessed by time of day - Average "
        @yAxis = "Total schools assesed"
        @graph = EnvironmentAssesment.filter(params).group_by_day_of_week(:created_at,format: "%a").count
        @graph_date_wise_avg = EnvironmentAssesment.filter(params).group_by_day(:created_at).count
        @graph_time_of_day_wise = EnvironmentAssesment.filter(params).group_by_hour_of_day(:created_at, format: "%-l %P").count
        total_schools_visited = EnvironmentAssesment.filter(params).count
        @results_day_wise = @graph.map { |key, value| { 'name' => key.to_s, 'y' => value }}
        @results_day_wise_avg = @graph.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/ total_schools_visited }}
        @results_time_of_day_wise = @graph_time_of_day_wise.map { |key, value| { 'name' => key.to_s, 'y' => value }}
        @results_time_of_day_wise_avg = @graph_time_of_day_wise.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/total_schools_visited }}
        @results_date_wise_avg = @graph_date_wise_avg.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/total_schools_visited }}

    when "total_child_screenings"
    #    @total_schools_assessed = School.total_schools_assessed.count
    # @total_child_screenings = ChildHealthScreening.count
    # @total_school_health_meetings = SchoolHealthMeeting.count
    # @total_followup_marks = FollowUp.count
    # @total_followups_not_completed_by_due_date =  FollowUp.late_followups.count
    # @total_followups_completed = FollowUp.completed.count
    # @total_parent_councelling_sessions = ParentCouncilingSession.count
    # @total_daily_plans_followed = DailyPlan.count
    # @total_daily_plans_not_followed = DailyPlan.not_followed.count


        @text_day_wise_sum = "Daily Child Screenings - Sum"
        @results_date_wise_avg = "Daily Child Screenings - Date Wise"
        @text_day_wise_sum_avg = "Daily Child Screenings - Average"
        @text_time_of_day_wise_sum = "Total Children Assessed by time of day - Sum "
        @text_time_of_day_wise_avg = "Total Children Assessed by time of day - Average "
        @yAxis = "Total Child Screenings"
        @graph = ChildHealthScreening.filter(params).group_by_day_of_week(:created_at,format: "%a").count
        @graph_date_wise_avg = ChildHealthScreening.filter(params).group_by_day(:created_at).count
        @graph_time_of_day_wise = ChildHealthScreening.filter(params).group_by_hour_of_day(:created_at, format: "%-l %P").count
        total_schools_visited = ChildHealthScreening.filter(params).count
        @results_day_wise = @graph.map { |key, value| { 'name' => key.to_s, 'y' => value }}
        @results_day_wise_avg = @graph.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/ total_schools_visited }}
        @results_time_of_day_wise = @graph_time_of_day_wise.map { |key, value| { 'name' => key.to_s, 'y' => value }}
        @results_time_of_day_wise_avg = @graph_time_of_day_wise.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/total_schools_visited }}
        @results_date_wise_avg = @graph_date_wise_avg.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/total_schools_visited }}

    when "total_school_health_meetings"
      
        @text_day_wise_sum = "Daily School Health Meeting - Sum"
        @results_date_wise_avg = "Daily School Health Meeting - Date Wise"
        @text_day_wise_sum_avg = "Daily School Health Meeting - Average"
        @text_time_of_day_wise_sum = "Total School Health Meetings by time of day - Sum "
        @text_time_of_day_wise_avg = "Total School Health Meetings by time of day - Average "
        @yAxis = "Total School Health Meeting"
        @graph = SchoolHealthMeeting.filter(params).group_by_day_of_week(:created_at,format: "%a").count
        @graph_date_wise_avg = SchoolHealthMeeting.filter(params).group_by_day(:created_at).count
        @graph_time_of_day_wise = SchoolHealthMeeting.filter(params).group_by_hour_of_day(:created_at, format: "%-l %P").count
        total_schools_visited = SchoolHealthMeeting.filter(params).count
        @results_day_wise = @graph.map { |key, value| { 'name' => key.to_s, 'y' => value }}
        @results_day_wise_avg = @graph.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/ total_schools_visited }}
        @results_time_of_day_wise = @graph_time_of_day_wise.map { |key, value| { 'name' => key.to_s, 'y' => value }}
        @results_time_of_day_wise_avg = @graph_time_of_day_wise.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/total_schools_visited }}
        @results_date_wise_avg = @graph_date_wise_avg.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/total_schools_visited }}
    
     when "total_follow_up_marks"
      
        @text_day_wise_sum = "Total Follow Ups Marks - Sum"
        @results_date_wise_avg = "Total Follow Ups Marks - Date Wise"
        @text_day_wise_sum_avg = "Total Follow Ups Marks - Average"
        @text_time_of_day_wise_sum = "Total Follow Ups Marked by time of day - Sum "
        @text_time_of_day_wise_avg = "Total Follow Ups Marked by time of day - Average "
        @yAxis = "Total Follow Ups Marked"
        @graph = FollowUp.filter(params).group_by_day_of_week(:created_at,format: "%a").count
        @graph_date_wise_avg = FollowUp.filter(params).group_by_day(:created_at).count
        @graph_time_of_day_wise = FollowUp.filter(params).group_by_hour_of_day(:created_at, format: "%-l %P").count
        total_schools_visited = FollowUp.filter(params).count
        @results_day_wise = @graph.map { |key, value| { 'name' => key.to_s, 'y' => value }}
        @results_day_wise_avg = @graph.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/ total_schools_visited }}
        @results_time_of_day_wise = @graph_time_of_day_wise.map { |key, value| { 'name' => key.to_s, 'y' => value }}
        @results_time_of_day_wise_avg = @graph_time_of_day_wise.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/total_schools_visited }}
        @results_date_wise_avg = @graph_date_wise_avg.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/total_schools_visited }}
    
      when "total_follow_up_late"
      
        @text_day_wise_sum = "Total Follow Ups Not Completed By Due Date - Sum"
        @results_date_wise_avg = "Total Follow Ups Not Completed By Due Date - Date Wise"
        @text_day_wise_sum_avg = "Total Follow Ups Not Completed By Due Date - Average"
        @text_time_of_day_wise_sum = "Total Follow Ups Not Completed By Due Date by time of day - Sum "
        @text_time_of_day_wise_avg = "Total Follow Ups Not Completed By Due Date by time of day - Average "
        @yAxis = "Total Follow Ups Not Completed By Due Date"
        @graph = FollowUp.late_followups.filter(params).group_by_day_of_week(:created_at,format: "%a").count
        @graph_date_wise_avg = FollowUp.late_followups.filter(params).group_by_day(:created_at).count
        @graph_time_of_day_wise = FollowUp.late_followups.filter(params).group_by_hour_of_day(:created_at, format: "%-l %P").count
        total_schools_visited = FollowUp.late_followups.filter(params).count
        @results_day_wise = @graph.map { |key, value| { 'name' => key.to_s, 'y' => value }}
        @results_day_wise_avg = @graph.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/ total_schools_visited }}
        @results_time_of_day_wise = @graph_time_of_day_wise.map { |key, value| { 'name' => key.to_s, 'y' => value }}
        @results_time_of_day_wise_avg = @graph_time_of_day_wise.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/total_schools_visited }}
        @results_date_wise_avg = @graph_date_wise_avg.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/total_schools_visited }}

      when "total_follow_up_completed"
      
        @text_day_wise_sum = "Total Follow Ups Completed - Sum"
        @results_date_wise_avg = "Total Follow Ups Completed  - Date Wise"
        @text_day_wise_sum_avg = "Total Follow Ups Completed  - Average"
        @text_time_of_day_wise_sum = "Total Follow Ups Completed  by time of day - Sum "
        @text_time_of_day_wise_avg = "Total Follow Ups Completed  by time of day - Average "
        @yAxis = "Total Follow Ups Completed "
        @graph = FollowUp.completed.filter(params).group_by_day_of_week(:created_at,format: "%a").count
        @graph_date_wise_avg = FollowUp.completed.filter(params).group_by_day(:created_at).count
        @graph_time_of_day_wise = FollowUp.completed.filter(params).group_by_hour_of_day(:created_at, format: "%-l %P").count
        total_schools_visited = FollowUp.completed.filter(params).count
        @results_day_wise = @graph.map { |key, value| { 'name' => key.to_s, 'y' => value }}
        @results_day_wise_avg = @graph.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/ total_schools_visited }}
        @results_time_of_day_wise = @graph_time_of_day_wise.map { |key, value| { 'name' => key.to_s, 'y' => value }}
        @results_time_of_day_wise_avg = @graph_time_of_day_wise.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/total_schools_visited }}
        @results_date_wise_avg = @graph_date_wise_avg.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/total_schools_visited }}

      when "parents_councelling_sessions"
      
        @text_day_wise_sum = "Total Parents Councelling Sessions - Sum"
        @results_date_wise_avg = "Total Parents Councelling Sessions  - Date Wise"
        @text_day_wise_sum_avg = "Total Parents Councelling Sessions  - Average"
        @text_time_of_day_wise_sum = "Total Parents Councelling Sessions by time of day - Sum "
        @text_time_of_day_wise_avg = "Total Parents Councelling Sessions by time of day - Average "
        @yAxis = "Total Parents Councelling Sessions"
        @graph = ParentCouncilingSession.filter(params).group_by_day_of_week(:created_at,format: "%a").count
        @graph_date_wise_avg = ParentCouncilingSession.filter(params).group_by_day(:created_at).count
        @graph_time_of_day_wise = ParentCouncilingSession.filter(params).group_by_hour_of_day(:created_at, format: "%-l %P").count
        total_schools_visited = ParentCouncilingSession.filter(params).count
        @results_day_wise = @graph.map { |key, value| { 'name' => key.to_s, 'y' => value }}
        @results_day_wise_avg = @graph.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/ total_schools_visited }}
        @results_time_of_day_wise = @graph_time_of_day_wise.map { |key, value| { 'name' => key.to_s, 'y' => value }}
        @results_time_of_day_wise_avg = @graph_time_of_day_wise.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/total_schools_visited }}
        @results_date_wise_avg = @graph_date_wise_avg.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/total_schools_visited }}
    
      when "daily_plans"
      
        @text_day_wise_sum = "Total Daily Plans - Sum"
        @results_date_wise_avg = "Total Daily Plans  - Date Wise"
        @text_day_wise_sum_avg = "Total Daily Plans  - Average"
        @text_time_of_day_wise_sum = "Total Daily Plans by time of day - Sum "
        @text_time_of_day_wise_avg = "Total Daily Plans by time of day - Average "
        @yAxis = "Total Daily Plans"
        @graph = DailyPlan.filter(params).group_by_day_of_week(:created_at,format: "%a").count
        @graph_date_wise_avg = DailyPlan.filter(params).group_by_day(:created_at).count
        @graph_time_of_day_wise = DailyPlan.filter(params).group_by_hour_of_day(:created_at, format: "%-l %P").count
        total_schools_visited = DailyPlan.filter(params).count
        @results_day_wise = @graph.map { |key, value| { 'name' => key.to_s, 'y' => value }}
        @results_day_wise_avg = @graph.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/ total_schools_visited }}
        @results_time_of_day_wise = @graph_time_of_day_wise.map { |key, value| { 'name' => key.to_s, 'y' => value }}
        @results_time_of_day_wise_avg = @graph_time_of_day_wise.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/total_schools_visited }}
        @results_date_wise_avg = @graph_date_wise_avg.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/total_schools_visited }}
    
      when "daily_plans_not_followed"
      
        @text_day_wise_sum = "Total Daily Plans - Sum"
        @results_date_wise_avg = "Total Daily Plans  - Date Wise"
        @text_day_wise_sum_avg = "Total Daily Plans  - Average"
        @text_time_of_day_wise_sum = "Total Daily Plans by time of day - Sum "
        @text_time_of_day_wise_avg = "Total Daily Plans by time of day - Average "
        @yAxis = "Total Daily Plans"
        @graph = DailyPlan.not_followed.filter(params).group_by_day_of_week(:created_at,format: "%a").count
        @graph_date_wise_avg = DailyPlan.not_followed.filter(params).group_by_day(:created_at).count
        @graph_time_of_day_wise = DailyPlan.not_followed.filter(params).group_by_hour_of_day(:created_at, format: "%-l %P").count
        total_schools_visited = DailyPlan.not_followed.filter(params).count
        @results_day_wise = @graph.map { |key, value| { 'name' => key.to_s, 'y' => value }}
        @results_day_wise_avg = @graph.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/ total_schools_visited }}
        @results_time_of_day_wise = @graph_time_of_day_wise.map { |key, value| { 'name' => key.to_s, 'y' => value }}
        @results_time_of_day_wise_avg = @graph_time_of_day_wise.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/total_schools_visited }}
        @results_date_wise_avg = @graph_date_wise_avg.map { |key, value| { 'name' => key.to_s, 'y' => value.to_f/total_schools_visited }}

    end

   


    respond_to do |format|
      format.js do 
      end
    end
  end


  def division_details

    @division = Division.find_by_id(params[:div])
    @districts = @division.districts
  end

  def district_details
    @district = District.find_by_id(params[:dist])
    @tehsils = @district.tehsils
  end

  def tehsil_details
    @tehsil = Tehsil.find_by_id(params[:teh])
    @schools = @tehsil.schools
  end

  # def school_details
  #   @school = School.find_by_id(params[:school])
  # end

  def logout
    reset_session
  	redirect_to({ :action=>'login' }) 
  end

  def create_new_division
    unless params[:name].blank?
      Division.create(name: params[:name])
      set_success_message "Division Successfully created!"
    else
      set_error_message "Division Name cannot be blank"
    end
    redirect_to :back
  end

  def create_new_school
    unless params[:name].blank? || params[:tehsil].blank?
      School.create(name: params[:name], tehsil_id: params[:tehsil], lat: params[:lat], long: params[:long])
      set_success_message "School Successfully created!"
    else
      set_error_message "School Name and Tehsil cannot be blank"
    end
    redirect_to :back
  end

  def manage_shns
    @links_list = {
        "Dahsboard" => admin_dashboard_path,
        "Manage SH&NS" => admin_manage_shns_path

    }
    @shns_list = Agent.filter_by_tehsil(Tehsil.filter_by_divisions(params[:division]).filter_by_districts(params[:district]).filter_by_tehsils(params[:tehsil])).paginate(:page => params[:page],:per_page => 15)


  end

  def create_new_shns
     
    
      unless params[:username].blank? || params[:password].blank? || params[:password] != params[:re_password]
        @username = Admin.where(email: params[:username]).present? || Agent.where(username: params[:username]).present?
        if @username.blank?
            @agent = Agent.create(username: params[:username], password: Digest::MD5.hexdigest(params[:password]),  first_name: params[:first_name],
              last_name: params[:last_name], tehsil_id: params[:tehsil], eamil: params[:email])
            set_success_message "Staff Member Successfully created!"
        else
            set_error_message "Username should be unique"
            redirect_to :back
            return
        end
      else
         set_error_message params[:password] == params[:re_password] ? "Username and Password cannot be blank" : "Password mismatch"
         redirect_to :back
        return
      end
      redirect_to admin_manage_shns_path
  end

  def create_new_staff
    unless params[:username].blank? || params[:password].blank? || params[:password] != params[:re_password]
      unless (params[:role].to_i > 2 && params[:tehsil].blank?) || (params[:role].to_i <= 2 && params[:district].blank? )
         @username = Admin.where(email: params[:username]).present? || Agent.where(username: params[:username]).present?
        if @username.blank?
            @new_staff = Admin.create(email: params[:username],  password: Digest::MD5.hexdigest(params[:password]), first_name: params[:first_name],
            last_name: params[:last_name])
            if params[:role].to_i > 2
              
              @new_staff.update(tehsil_id: params[:tehsil],district_id: Tehsil.find_by_id(params[:tehsil]).district_id)
            else
              @new_staff.update(district_id: params[:district])
            end

              #         ROLES_FOR_STAFF = {
              #   "COS" => 1,
              #   "DOS" => 2,
              #   "DOO" => 3,
              #   "DU" => 4
              # }
            case params[:role].to_i
            when 1
              @new_staff.add_role :cos
            when 2
              @new_staff.add_role :dos
            when 3
              @new_staff.add_role :doo
            when 4
              @new_staff.add_role :du
            end

            @new_staff.save!

            set_success_message "Staff Member Successfully created!"
        else
            set_error_message "Username should be unique"
            redirect_to :back
            return
        end
      else
        set_error_message "Please assign an area for the staff"
        redirect_to :back
        return

      end
     
    else
      set_error_message params[:password] == params[:re_password] ? "Email and Password cannot be blank" : "Password mismatch"
       redirect_to :back
        return
    end
  redirect_to admin_manage_staff_path
  end

  def manage_staff
     @links_list = {
        "Dahsboard" => admin_dashboard_path,
        "Manage SH&NS" => admin_manage_staff_path
    }
    @staff = Admin.load_all_but_super_admin.filter_by_divisions(params[:division]).filter_by_districts(params[:district]).filter_by_tehsils(params[:tehsil]).filter_by_roles(params[:role])
  end

  def new_staff_member
     @links_list = {
        "Dahsboard" => admin_dashboard_path,
        "Manage Staff" => admin_manage_staff_path,
        "Create New Staff Member" => admin_new_staff_member_path
    }
  end

   def new_shns_member
     @links_list = {
        "Dahsboard" => admin_dashboard_path,
        "Manage SH&NS" => admin_manage_shns_path,
        "Create New SH&NS" => admin_create_new_shns_path
    }
  end

  def update_staff_status
    @staff = Admin.find_by_id(params[:id])
    if params[:status] == "true"
      @staff.activate
    else
       @staff.deactivate
    end

    respond_to do |format|
      format.js do
        render :text => "Success"
      end
    end
  end

  def create_new_district
    unless params[:name].blank? || params[:division].blank?
      District.create(name: params[:name], division_id: params[:division])
      set_success_message "District Successfully created!"
    else
      set_error_message "District name and division cannot be blank"
    end
  redirect_to :back
  end
 
  def create_new_tehsil
		unless params[:name].blank? || params[:district].blank? 
			Tehsil.create(name: params[:name], district_id: params[:district])
	 		set_success_message "Tehsil Successfully created!"
	 	else
	 		set_error_message "Tehsil name and district cannot be blank"
		end
 	redirect_to :back
  end

  def manage_divisions
     @links_list = {
        "Dahsboard" => admin_dashboard_path,
        "Manage Divisions" => admin_manage_divisions_path,
    }
    @divisions = Division.paginate(:page => params[:page],:per_page => 15)
  end

  def manage_districts

     @links_list = {
        "Dahsboard" => admin_dashboard_path,
        "Manage Districts" => admin_manage_districts_path,
    }
    @districts = District.filter_by_divisions(params[:division])
    if params[:division].present?
        @division = Division.find_by_id(params[:division])
    end
  end

  def manage_schools

     @links_list = {
        "Dahsboard" => admin_dashboard_path,
        "Manage Schools" => admin_manage_schools_path,
    }
     @schools = School.where("`name` like '%#{params[:q]}%'").paginate(:page => params[:page],:per_page => 15)

  end

  def school_details

     @links_list = {
        "Dahsboard" => admin_dashboard_path,
        "Manage Schools" => admin_manage_schools_path,
        "School Details" => admin_school_details_path(school: params[:school])
    }
     @school = School.find_by_id(params[:school])
     @env_assesments = @school.environment_assesments
     @std_assesments = ChildHealthScreening.where(student_id: @school.students.ids)
     @health_sessions = HealthEducationSession.where(owner_id: @school.id, owner_type: "School")
     @council_meetings = CouncilMeeting.where(school_id: @school.id)

  end

  def env_assesment_details
    @env = EnvironmentAssesment.find_by_id(params[:e]) || EnvironmentAssesment.new
  end

    def council_meetings_details
    @cm = CouncilMeeting.find_by_id(params[:e]) || CouncilMeeting.new
  end

  def health_sessions_details
    @h_session = HealthEducationSession.find_by_id(params[:e]) || HealthEducationSession.new
  end

  def std_assesment_details
    @std = ChildHealthScreening.find_by_id(params[:s])
  end

  def manage_tehsils
     @links_list = {
        "Dahsboard" => admin_dashboard_path,
        "Manage Tehsils" => admin_manage_tehsils_path,
    }
    @tehsils = Tehsil.filter_by_divisions(params[:division]).filter_by_districts(params[:district]).paginate(:page => params[:page],:per_page => 15)
    if params[:division].present?
        @division = Division.find(params[:division])
    end
    if params[:district].present?
        @district = District.find(params[:district])
    end
  end

  def manage_ucs
     @links_list = {
        "Dahsboard" => admin_dashboard_path,
        "Manage UCs" => admin_manage_ucs_path,
    }
  	@ucs = UnionConcil.filter_by_divisions(params[:division]).filter_by_districts(params[:district]).filter_by_tehsils(params[:tehsil]).paginate(:page => params[:page],:per_page => 15)
  end

  def delete_division
    unless params[:divi_id].blank? || @admin.is_not_super_admin? 
      Division.find_by_id(params[:divi_id]).delete
      set_success_message "Division deleted Successfully"
    else
      set_error_message "Action not allowed"
    end
  redirect_to :back
  end 

  def edit_division
      @div = Division.find_by_id(params[:divi_id])
      respond_to do |format|
      format.js do
      end
    end
  end 

  def edit_district
      @dis = District.find_by_id(params[:dis])
      respond_to do |format|
      format.js do
      end
    end
  end 

  def edit_staff
      @staff = Admin.find_by_id(params[:staff_id])
      respond_to do |format|
      format.js do
      end
    end
  end 


  def edit_tehsil
      @tehsil = Tehsil.find_by_id(params[:tehsil_id])
      respond_to do |format|
      format.js do
      end
    end
  end 

  def edit_uc
      @uc = UnionConcil.find_by_id(params[:uc_id])
      respond_to do |format|
      format.js do
      end
    end
  end 

  def update_division
      @div = Division.find_by_id(params[:div])
      @div.update(name: params[:name])
    redirect_to :back
  end

  def update_staff
      @staff = Admin.find_by_id(params[:staff_id])
      @staff.update(email: params[:username], first_name: params[:first_name],
        last_name: params[:last_name])

      if params[:password].present?
        @staff.update( password: Digest::MD5.hexdigest(params[:password]))
      end
    redirect_to :back
  end

  def update_tehsil
      @tehsil = Tehsil.find_by_id(params[:tehsil_id])
      @tehsil.update(name: params[:name], district_id: params[:district])
    redirect_to :back
  end



  def update_uc
      @uc = UnionConcil.find_by_id(params[:uc_id])
      @uc.update(name: params[:name], tehsil_id: params[:tehsil])
    redirect_to :back
  end


  def update_district
      @dis = District.find_by_id(params[:dis])
      @dis.update(name: params[:name], division_id: params[:division])
    redirect_to :back
  end

  def delete_school
    unless params[:school_id].blank? || @admin.is_not_super_admin? 
      School.find_by_id(params[:school_id]).delete
      set_success_message "School deleted Successfully"
    else
      set_error_message "Action not allowed"
    end
  redirect_to :back
  end 

  def delete_staff
    unless params[:staff_id].blank? || @admin.is_not_super_admin? 
      Admin.find_by_id(params[:staff_id]).delete
      set_success_message "Member deleted Successfully"
    else
      set_error_message "Action not allowed"
    end
  redirect_to :back
  end 

   def delete_tehsil
    unless params[:teh_id].blank? || @admin.is_not_super_admin? 
      Tehsil.find_by_id(params[:teh_id]).delete
      set_success_message "Tehsil deleted Successfully"
    else
      set_error_message "Action not allowed"
    end
  redirect_to :back
  end 

  def delete_district
   	unless params[:dis_id].blank? || @admin.is_not_super_admin? 
   		District.find_by_id(params[:dis_id]).delete
   		set_success_message "District deleted Successfully"
   	else
   		set_error_message "Action not allowed"
   	end
 	redirect_to :back
  end	

  def save_settings
	require 'digest/md5'
		unless params[:password].blank?
	 		@admin.update(first_name: params[:firstName] , last_name: params[:lastName], password: Digest::MD5.hexdigest(params[:password]))	
	 		set_success_message "Settings Updated Successfully!"
	 	else
	 		set_error_message "Password cannot be blank"
		end
 	redirect_to :back
  end
  
  def settings

  	
  end


  def session_login
  	@admin = Admin.login(params[:email], params[:password])
      if @admin.blank? 
         redirect_to({ :action=>'login' }, alert: 'Information you entered is wrong.') 
         return
      elsif @admin.present?
          if @admin.first.is_active? 
      	   set_session @admin.first
            redirect_to({ :action=>'dashboard' })
            return
          else
            redirect_to({ :action=>'login' }, alert: 'Your account is locked.') 
           end
      return
     end
  end

  def dashboard
    @students_assesed_today = Student.total_students_assessed.count
    @total_schools_visited  = School.total_schools_visited.count
    @total_students_assessed_yesterday = Student.total_students_assessed_yesterday.count
    @health_sessions_conducted_today = HealthEducationSession.sessions_conducted_today.count
    @active_agents = Agent.active_agents_today.count
    @total_agents = Agent.all.count
    @follow_ups_today = FollowUp.follow_ups_today
    @completed_followups = @follow_ups_today.completed.count
    @follow_ups_today = @follow_ups_today.count
  end

  def new_agent

  end

  def export_data
     @links_list = {
        "Dahsboard" => admin_dashboard_path,
        "Export Data" => admin_export_data_path,
    }
    @tehsils= Tehsil.all.limit(10)
    @schools = School.all.limit(10)
    @districts = District.all.limit(10)
  end

  def community_daily_reporting
    @tehsils= Tehsil.all
    @schools = School.all.limit(10)
    @districts = District.all
    @shns = Agent.all
  end

  def monthly_school_visits
    @tehsils= Tehsil.all
    @schools = School.all.limit(10)
    @districts = District.all
    @shns = Agent.all
  end

  def daily_school_visits
     @links_list = {
        "Dahsboard" => admin_dashboard_path,
        "Export Reports" => admin_daily_school_visits_path,
    }
    @tehsils= Tehsil.all
    @schools = School.all.limit(10)
    @districts = District.all
    @shns = Agent.all
  end



  def track_agents
     @links_list = {
        "Dahsboard" => admin_dashboard_path,
        "Staff Performance" => admin_track_agents_path,
    }
    @total_schools_assessed = EnvironmentAssesment.count
    @total_child_screenings = ChildHealthScreening.count
    @total_school_health_meetings = SchoolHealthMeeting.count
    @total_followup_marks = FollowUp.count
    @total_followups_not_completed_by_due_date =  FollowUp.late_followups.count
    @total_followups_completed = FollowUp.completed.count
    @total_parent_councelling_sessions = ParentCouncilingSession.count
    @total_daily_plans_followed = DailyPlan.count
    @total_daily_plans_not_followed = DailyPlan.not_followed.count
    @latLongs = [{lat: 30.3753, lng: 69.3451}, {lat: 31.3753, lng: 69.3451}, {lat: 30.37544, lng: 69.3451},{lat: 30.3763, lng: 69.3451}, {lat: 31.3773, lng: 69.3451}, {lat: 30.37864, lng: 69.3451}]

    if params[:type].to_i == MAP_TYPE_REV[:agent]
      if params[:agent].present? 
        locations = LocationHistory.where(agent_id: params[:agent])
      else
        locations = LocationHistory.all
      end
    elsif params[:type].to_i == MAP_TYPE_REV[:followups]
      locations = FollowUp.all
    elsif params[:type].to_i == MAP_TYPE_REV[:env]
      ids = EnvironmentAssesment.all.pluck(:school_id)
      locations = School.where(id: ids)
    elsif params[:type].to_i == MAP_TYPE_REV[:health_sessions]
      locations = HealthEducationSession.all
    end
     if locations.present? 
      @latLongs = locations.map {|fol| { lat: fol.lat.try(:to_f), lng: fol.long.try(:to_f) }}
    end
  end

  private

  def set_session admin
   session[:new_check] = true
   session[:admin] = admin.id
  end

  def authenticate_admin
   if session[:admin].blank? || session[:new_check].blank?
      reset_session
      redirect_to({ :action=>'login' }, notice: 'Please login to conitnue.') 
   end
  end 

  def load_admin
    @links_list = {}
  	@admin = Admin.find_by_id(session[:admin])
  end 

  def set_error_message message
  	flash[:alert] = message
  	flash[:alert_class] = "alert-danger"
	flash[:alert_start] = "Error"
  end

  def set_success_message message
  	    flash[:alert] = message
  		flash[:alert_class] = "alert-success"
 		flash[:alert_start] = "Success"
  end

end
