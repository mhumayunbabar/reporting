class SchoolHealthMeeting < ActiveRecord::Base
	belongs_to :school
	def self.filter params
		start = params[:range].split(' - ')[0]
		end_date = params[:range].split(' - ')[1]
		range_start = start + " " +params[:start_time]
		range_end = end_date + " " +params[:end_time]
			result = self.all
		if start.present? && end_date.present?
			result = where(created_at: [ DateTime.strptime(range_start, "%m/%d/%Y %I:%M %p").. DateTime.strptime(range_end, "%m/%d/%Y %I:%M %p")])
		end

		if params[:district].present?
			result = result.where(school_id: School.where(tehsil_id: Tehsil.where(district_id: params[:district])))
		end

		if params[:tehsil].present?
			result = result.where(school_id: School.where(tehsil_id: params[:tehsil]))
		end

		if params[:hsns].present?
			result = result.where(agent_id: params[:shns])
		end

		result
	end
end
