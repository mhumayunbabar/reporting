class DailyPlan < ActiveRecord::Base
	def self.not_followed
		where(completed_date: nil)
	end

	def self.followed
		where('completed_date == due_date')
	end

	def self.filter_by_range params
		start = params[:range].split(' - ')[0]
		end_date = params[:range].split(' - ')[1]
		if params[:start_time].blank?
			params[:start_time] = "12:00 am"
		end
		
		if params[:end_time].blank?
			params[:end_time] = "11:59 pm"
		end
		range_start = start + " " +params[:start_time]
		range_end = end_date + " " +params[:end_time]

		where(due_date: [ DateTime.strptime(range_start, "%m/%d/%Y %I:%M %p").. DateTime.strptime(range_end, "%m/%d/%Y %I:%M %p")])
	end

end
