class UnionConcil < ApplicationRecord
	belongs_to :tehsil

	def division_name
		tehsil.division_name
	end

 	def district_name
 		tehsil.district_name
 	end

	def tehsil_name
		tehsil.name
	end

	def self.filter_by_tehsils tehsil_id
 		if tehsil_id.present? 
			where(tehsil_id: tehsil_id) 
		else
			self.all
		end
 	end

 	def self.filter_by_districts district
 		if district.present? 
			where(tehsil_id: Tehsil.where(district_id: district).ids) 
		else
			self.all
		end
 	end


 	def self.filter_by_divisions division
		if division.present? 
			where(tehsil_id: Tehsil.where(district_id: District.where(division_id: division).ids).ids)
		else
			self.all
		end
	end		
	
end
