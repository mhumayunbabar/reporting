class FollowUp < ApplicationRecord
	belongs_to :owner, polymorphic: true, dependent: :destroy
	def self.follow_ups_today
		where(schedule_date: Date.today)
	end

	def self.completed
		where(completed: true)
	end

	def self.late_followups
		where("completed_date > schedule_date")
	end

	def self.filter params
		start = params[:range].split(' - ')[0]
		end_date = params[:range].split(' - ')[1]
		if params[:start_time].blank?
			params[:start_time] = "12:00 am"
		end
		
		if params[:end_time].blank?
			params[:end_time] = "11:59 pm"
		end
		range_start = start + " " +params[:start_time]
		range_end = end_date + " " +params[:end_time]
			result = self.all
		if start.present? && end_date.present?
			result = where(created_at: [ DateTime.strptime(range_start, "%m/%d/%Y %I:%M %p").. DateTime.strptime(range_end, "%m/%d/%Y %I:%M %p")])
		end

		if params[:district].present?
			result = result.where(owner_type: "Student" ,owner_id: Student.where(school_id: School.where(tehsil_id: Tehsil.where(district_id: params[:district]).ids).ids))
		end

		if params[:tehsil].present?
			result = result.where(owner_type: "Student" ,owner_id: Student.where(school_id: School.where(tehsil_id: params[:tehsil])))
		end


		if params[:hsns].present?
			result = result.where(agent_id: params[:shns])
		end

		result
	end
end
