class Admin < ApplicationRecord
   belongs_to :district
   belongs_to :tehsil
   rolify 
	ROLES = %i[super_admin COS DOS DOO DU ]

	def before_add_method(role)
	    # do something before it gets added
	end

	def self.login uName, password
		require 'digest/md5'
		self.where(email: uName, password: Digest::MD5.hexdigest(password))
	end

	def self.create_dummy
		require 'digest/md5'
		create(email: "admin@reporitng", password: Digest::MD5.hexdigest("^-xzar4"))
	end

	def name
		first_name.to_s + " " + last_name.to_s
	end

	def is_not_super_admin? 
		!(self.has_role? :super_admin)
	end

	def role_name
		roles.last.name
	end

	def place_name
		if tehsil.present? 
			tehsil.name_with_district
		else
			district.try(:name) || "No Area Assigned"
		end
	end

	def self.load_all_but_super_admin
		where.not(id: 1)
	end

	def self.filter_by_divisions division_id
		if division_id.present?
			self.where(district_id: District.where(division_id: division_id).ids)
		else
			self.all
		end
	end

	def self.filter_by_districts district_id
		if district_id.present?
			self.where(district_id: district_id)
		else
	    	self.all
		end
	end

	def self.filter_by_tehsils tehsil_id
		if tehsil_id.present?
			self.where(tehsil_id: tehsil_id)
		else
			self.all
		end
	end

	def self.filter_by_roles role_number
		if role_number.present?
			case role_number.to_i
	        when 1
	          self.with_role(:cos)
	        when 2
	        	self.with_role(:dos)
	        when 3
	        	self.with_role(:doo)
	        when 4
	        	self.with_role(:du)
	        else
	        	self.all
	        end
	    else
	    	self.all
	    end
	end

	def activate
		self.update(state: ADMIN_STATE[:active])
	end

	def deactivate
		self.update(state: ADMIN_STATE[:inactive])
	end

	def get_status_html
		state == ADMIN_STATE[:active] ? "checked" : ""
	end

	def is_active? 
		state == ADMIN_STATE[:active]
	end

	 
end
