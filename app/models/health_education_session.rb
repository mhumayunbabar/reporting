class HealthEducationSession < ApplicationRecord

	def self.sessions_conducted_today
		where(created_at: Date.today)
	end

end
