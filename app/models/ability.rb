class Ability
  include CanCan::Ability

  def initialize(user)
    can :manage, :all if user.has_role? :super_admin
  
    
  end
end
