class Tehsil < ApplicationRecord
	belongs_to :district
	has_many :agents
	has_many :union_concils
	has_many :schools
	def name_with_district
		district.name + " / " + name
	end

	def self.get_ucs 
		UnionConcil.where(tehsil_id: self.ids)
	end

	def ucs_count
		union_concils.count
	end

	def schools_count
		schools.count
	end

	def self.get_schools
		School.where(tehsil_id: self.ids)
	end

	def division_name
		district.division_name
	end

 	def district_name
 		district.name
 	end

 	def self.filter_by_districts district
 		if district.present? 
			where(district_id: district)
		else
			self.all
		end
 	end
	## make this better
 	def self.filter_by_tehsils teshil
 		if teshil.present?
 			where(id: teshil)
 		else
 			self.all
 		end
 	end

 	def self.filter_by_divisions division
		if division.present? 
			where(district_id: District.where(division_id: division).ids)
		else
			self.all
		end
	end		
end
