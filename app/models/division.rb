class Division < ApplicationRecord
	has_many :districts
	def districts_count
		districts.count
	end
		
	def tehsils_count
		districts.get_tehsils.count
	end
	
	def ucs_count
		districts.get_tehsils.get_ucs.count
	end
	
	def schools_count
		districts.get_tehsils.get_schools.count
	end

	# sub trees

end
