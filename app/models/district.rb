class District < ApplicationRecord
	belongs_to :division
	has_many :tehsils
	def name_with_division
		division.name + " / " + name
	end

	def self.get_tehsils 
		Tehsil.where(district_id: self.ids)
	end

	def tehsils_count
		tehsils.count
	end
	
	def ucs_count
		tehsils.get_ucs.count
	end
	
	def schools_count
		tehsils.get_schools.count
	end


	def division_name
		division.name
	end

	def self.filter_by_divisions division
		if division.present? 
			where(division_id: division)
		else
			self.all
		end
	end



end
