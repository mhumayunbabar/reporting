class ChildHealthScreening < ApplicationRecord
	belongs_to :student
	def self.filter_by_range start_date, end_date, params
		start_date = params[:range].split(' - ')[0]
		end_date = params[:range].split(' - ')[1]
		if params[:start_time].blank?
			params[:start_time] = "12:00 am"
		end
		
		if params[:end_time].blank?
			params[:end_time] = "11:59 pm"
		end


		if(start_date.present? &&  end_date.present?)
			where(updated_at: [start_date.to_date..end_date.to_date])
		else
			where(1)
		end
	end

	def self.filter params
		start = params[:range].split(' - ')[0]
		end_date = params[:range].split(' - ')[1]
		if params[:start_time].blank?
			params[:start_time] = "12:00 am"
		end
		
		if params[:end_time].blank?
			params[:end_time] = "11:59 pm"
		end

		range_start = start + " " +params[:start_time] 
		range_end = end_date + " " +params[:end_time] 
			result = self.all
		if start.present? && end_date.present?
			result = where(created_at: [ DateTime.strptime(range_start, "%m/%d/%Y %I:%M %p").. DateTime.strptime(range_end, "%m/%d/%Y %I:%M %p")])
		end

		if params[:district].present?
			result = result.where(student_id: Student.where(school_id: School.where(tehsil_id: Tehsil.where(district_id: params[:district]).ids).ids))
		end

		if params[:tehsil].present?
			result = result.where(student_id: Student.where(school_id: School.where(tehsil_id: params[:tehsil])))
		end

		if params[:hsns].present?
			result = result.where(agent_id: params[:shns])
		end

		result
	end

	# ----------------- #
	
	def self.male
		self.joins(:student).where('`students`.gender = 0')
	end
	
	def self.female
		self.joins(:student).where('`students`.gender = 1')
	end

    def self.bmi
    	where("bmi < 18.5 OR bmi > 25")
    end

    def self.over_bmi
    	where("bmi > 25")
    end

    def self.under_bmi
    	where("bmi < 18.5")
    end

    def self.anemia
    	where(anemia: true)
    end

    def self.physical_deformities
    	where(physical_deformities: true)
    end

    def self.hearing_problem
    	where(hearing_problem: true)
    end

	def self.ear_problem
		where(ear_problem: true)
	end
    	
	def self.vision
		where(vision: true)
	end
	
	def self.eye_problem
		where(eye_problem: true)
	end
	
	def self.respiratory_infection
		where(respiratory_infection: true)
	end
	
	def self.skin_problem
		where(skin_problem: true)
	end
	
	def self.trauma_physical_injuries
		where(trauma_physical_injuries: true)
	end
	
	def self.jaundice
		where(jaundice: true)
	end
	
	def self.facial_puffiness
		where(facial_puffiness: true)
	end
	
	def self.edema
		where(edema: true)
	end
	
	def self.mental_dissability
		where(mental_dissability: true)
	end
	
	def self.ho_immunisation
		where(ho_immunisation: true)
	end
	
	def self.ho_allergy
		where(ho_allergy: true)
	end
	
	def self.ho_hospitalisation
		where(ho_hospitalisation: true)
	end

	def self.dental_problem
		where(dental_problem: true)
	end
	def self.body_nits
		where(body_nits: true)
	end
	def self.lice_in_hair
		where(lice_in_hair: true)
	end
	
	def agent_name
	Agent.find_by_id(agent_id).name
	end 
end
