class Agent < ApplicationRecord
	belongs_to :tehsil
	has_many :students
	has_many :child_health_screenings
	has_many :health_education_sessions
	has_many :follow_ups
	has_many :location_histories
	has_many :daily_plans

	def name
		first_name.to_s + " " + last_name.to_s
	end

	def self.login uName, password
		require 'digest/md5'
		self.where(username: uName, password: Digest::MD5.hexdigest(password)).first
	end

	def self.create_dummy
		require 'digest/md5'
		create(username: "dummy", password: Digest::MD5.hexdigest("dummy"))

		
	end


	def self.filter_by_tehsil tehsil
		where(tehsil_id: tehsil)
	end

	def place_name
		tehsil.name_with_district
	end

	def get_followups date
		follow_ups.where(schedule_date: date).count
	end

	def self.active_agents_today
		where(last_active: Date.today)
	end
end
