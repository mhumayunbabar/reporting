class Student < ApplicationRecord
	has_many :child_health_screenings
	acts_as_xlsx

	def self.select_params params
		select(params)
	end

	def self.total_students_assessed
		where(last_assessed: Date.today)
	end

	def self.male
		where(gender: 0)
	end
	
	def self.female
		where(gender: 1)
	end

	def self.total_students_assessed_yesterday
		where(last_assessed: 1.day.ago)
	end

	def get_child_heath_screening
		child_health_screenings.last
	end

	def self.create_excel_sheet params, sheet
		start_date = params[:range].split(" - ")[0]
		end_date = params[:range].split(" - ")[1]
		schools = []
		if params[:school].present?
			schools << params[:school].to_i
		elsif params[:tehsil].present?
			schools << School.where(tehsil_id: params[:tehsil]).pluck(:id)
		elsif params[:district].present?
			tehsils = Tehsil.where(district_id: params[:district]).pluck(:id)
			schools << School.where(tehsil_id: tehsils).pluck(:id)

		end
			
		schools.flatten!
	if schools.present?
			self.where(school_id: schools).each do |std|
				row = []
				flag = false
				cols = params[:cols].split(',')
				health_params = params[:cols_health].split(',')
				cols.each do |col|
					row << std[col]
				end

				health_params.each do |health_col|
					if std.child_health_screenings.filter_by_range(start_date, end_date, params).present?
						flag = true
						row << std.child_health_screenings.filter_by_range(start_date, end_date, params).last[health_col] 
					else
						row << ''
					end
				end
				if flag
					sheet.add_row row
				end
			end
		end

	end

	def self.create_excel params

		start_date = params[:range].split(" - ")[0]
		end_date = params[:range].split(" - ")[1]
		sheet = []
		schools = []
		if params[:school].present?
			schools << params[:school].to_i
		elsif params[:tehsil].present?
			schools << School.where(tehsil_id: params[:tehsil]).pluck(:id)
		elsif params[:district].present?
			tehsils = Tehsil.where(district_id: params[:district]).pluck(:id)
			schools << School.where(tehsil_id: tehsils).pluck(:id)

		end
			
		schools.flatten!
	if schools.present?
			self.where(school_id: schools).each do |std|
				row = []
				flag = false
				cols = params[:cols]
				health_params = params[:cols_health]
				cols.each do |col|
					row << std[col]
				end

				health_params.each do |health_col|
					if std.child_health_screenings.filter_by_range(start_date, end_date).present?
						flag = true
						row << std.child_health_screenings.filter_by_range(start_date, end_date).last[health_col] 
					else
						row << ''
					end
				end
				if flag
					sheet << row
				end
			end
		end
	sheet
	end

	def self.filter_by_range_assesed params
		start = params[:range].split(' - ')[0]
		end_date = params[:range].split(' - ')[1]
		if params[:start_time].blank?
			params[:start_time] = "12:00 am"
		end
		
		if params[:end_time].blank?
			params[:end_time] = "11:59 pm"
		end

		range_start = start + " " +params[:start_time] 
		range_end = end_date + " " +params[:end_time] 
		if start.present? && end_date.present?
			result = where(last_assessed: [ DateTime.strptime(range_start, "%m/%d/%Y %I:%M %p").. DateTime.strptime(range_end, "%m/%d/%Y %I:%M %p")])
		end
	end

end
