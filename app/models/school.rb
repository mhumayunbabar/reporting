class School < ApplicationRecord
	belongs_to :tehsil
	has_many :students
	has_many :environment_assesments
	acts_as_xlsx
	ransacker :name_case_insensitive, type: :string do
      arel_table[:name].lower
    end

	def self.by_tehsil tehsil
		where(tehsil_id: tehsil)
	end

	def self.total_schools_assessed
		where.not(last_visited: nil)
	end
	
	def self.total_schools_visited
		where(last_visited: Date.today)
	end

	def self.create_excel params
		resutls = []
		schools = []

		start_date = params[:range].split(" - ")[0]
		end_date = params[:range].split(" - ")[1]
		if params[:school].present?
			schools << params[:school].to_i
		elsif params[:tehsil].present?
			schools << School.where(tehsil_id: params[:tehsil]).pluck(:id)
		elsif params[:district].present?
			tehsils = Tehsil.where(district_id: params[:district]).pluck(:id)
			schools << School.where(tehsil_id: tehsils).pluck(:id)
		end
			
	schools.flatten!
	if schools.present?
			self.where(id: schools).each do |school|
				row = []
				cols = params[:cols]
				flag = false
				health_params = params[:cols_health]
				env_params = params[:cols_env]
				cols.each do |col|
					row << school[col]
				end

				health_res = School.get_health_params school.id, health_params, params, start_date, end_date
				row += health_res
				row.flatten!

				env_params.each do |evn_col|
					if school.environment_assesments.present?
						row << school.environment_assesments.last[evn_col] 
					else
						row << 'NA'
					end
				end
					resutls << row

			end
		end
			resutls
		
	end

		def self.create_excel_sheet params, sheet

		schools = []
		start_date = params[:range].split(" - ")[0]
		end_date = params[:range].split(" - ")[1]
		if params[:school].present?
			schools << params[:school].to_i
		elsif params[:tehsil].present?
			schools << School.where(tehsil_id: params[:tehsil]).pluck(:id)
		elsif params[:district].present?
			tehsils = Tehsil.where(district_id: params[:district]).pluck(:id)
			schools << School.where(tehsil_id: tehsils).pluck(:id)
		end
			
	schools.flatten!
		if schools.present?
				self.where(id: schools).each do |school|
					row = []
					cols = params[:cols].split(',')
					flag = false
					health_params = params[:cols_health].split(',')
					env_params = params[:cols_env].split(',')
					cols.each do |col|
						row << school[col]
					end

					health_res = School.get_health_params school.id, health_params, params, start_date, end_date
					row += health_res
					row.flatten!

					env_params.each do |evn_col|
						if school.environment_assesments.present?
							row << school.environment_assesments.last[evn_col] 
						else
							row << 'NA'
						end
					end
						sheet.add_row row

				end
			end

		end

	def self.get_health_params schools, health_cols, params, start_date, end_date
	 	lcl_students = Student.where(school_id: schools).ids
	 	healths = ChildHealthScreening.filter_by_range(start_date, end_date).where(student_id: lcl_students)
	 	row = []
	 	health_cols.each do |health_col|
	 		n = healths.pluck(health_col).select {|d| d }
	 		row << n.count
	 	end
	 	row
	end

	def self.filter_by_range_visited params
		start = params[:range].split(' - ')[0]
		end_date = params[:range].split(' - ')[1]
		if params[:start_time].blank?
			params[:start_time] = "12:00 am"
		end
		
		if params[:end_time].blank?
			params[:end_time] = "11:59 pm"
		end

		range_start = start + " " +params[:start_time] 
		range_end = end_date + " " +params[:end_time] 
		if start.present? && end_date.present?
			result = where(last_visited: [ DateTime.strptime(range_start, "%m/%d/%Y %I:%M %p").. DateTime.strptime(range_end, "%m/%d/%Y %I:%M %p")])
		end
	end

	def division_name
		tehsil.division_name
	end

	def district_name
		tehsil.district_name
	end

	def tehsil_name
		tehsil.name
	end
end
