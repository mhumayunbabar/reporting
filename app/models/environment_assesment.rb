class EnvironmentAssesment < ApplicationRecord
	belongs_to :school
	def self.filter params
		start = params[:range].split(' - ')[0]
		end_date = params[:range].split(' - ')[1]
		range_start = start + " " +params[:start_time]
		range_end = end_date + " " +params[:end_time]
			result = self.all
		if start.present? && end_date.present?
			result = where(created_at: [ DateTime.strptime(range_start, "%m/%d/%Y %I:%M %p").. DateTime.strptime(range_end, "%m/%d/%Y %I:%M %p")])
		end

		if params[:district].present?
			result = result.where(school_id: School.where(tehsil_id: Tehsil.where(district_id: params[:district])))
		end

		if params[:tehsil].present?
			result = result.where(school_id: School.where(tehsil_id: params[:tehsil]))
		end

		if params[:hsns].present?
			result = result.where(agent_id: params[:shns])
		end

		result
	end

	def self.condition_of_building_good
		where(condition_of_building: ENVIRONMENT_VAR[:good])
	end

	def self.condition_of_building_satisfactory
		where(condition_of_building: ENVIRONMENT_VAR[:satisfactory])
	end

	def self.condition_of_building_poor
		where(condition_of_building: ENVIRONMENT_VAR[:poor])
	end

def self.cleanliness_of_school_environment_good
	where(cleanliness_of_school_environment: ENVIRONMENT_VAR[:good])
end

def self.cleanliness_of_school_environment_satisfactory
	where(cleanliness_of_school_environment: ENVIRONMENT_VAR[:satisfactory])
end

def self.cleanliness_of_school_environment_poor
	where(cleanliness_of_school_environment: ENVIRONMENT_VAR[:poor])
end

def self.class_room_space_adequacy_good
	where(class_room_space_adequacy: ENVIRONMENT_VAR[:good])
end

def self.class_room_space_adequacy_satisfactory
	where(class_room_space_adequacy: ENVIRONMENT_VAR[:satisfactory])
end

def self.class_room_space_adequacy_poor
	where(class_room_space_adequacy: ENVIRONMENT_VAR[:poor])
end

def self.availability_of_safe_drinking_water_good
	where(availability_of_safe_drinking_water: ENVIRONMENT_VAR[:good])
end

def self.availability_of_safe_drinking_water_satisfactory
	where(availability_of_safe_drinking_water: ENVIRONMENT_VAR[:satisfactory])
end

def self.availability_of_safe_drinking_water_poor
	where(availability_of_safe_drinking_water: ENVIRONMENT_VAR[:poor])
end

def self.sanitary_condition_of_toilets_good
	where(sanitary_condition_of_toilets: ENVIRONMENT_VAR[:good])
end

def self.sanitary_condition_of_toilets_satisfactory
	where(sanitary_condition_of_toilets: ENVIRONMENT_VAR[:satisfactory])
end

def self.sanitary_condition_of_toilets_poor
	where(sanitary_condition_of_toilets: ENVIRONMENT_VAR[:poor])
end

def self.drainage_condiiton_good
	where(drainage_condiiton: ENVIRONMENT_VAR[:good])
end

def self.drainage_condiiton_satisfactory
	where(drainage_condiiton: ENVIRONMENT_VAR[:satisfactory])
end

def self.drainage_condiiton_poor
	where(drainage_condiiton: ENVIRONMENT_VAR[:poor])
end

def self.waste_management_good
	where(waste_management: ENVIRONMENT_VAR[:good])
end

def self.waste_management_satisfactory
	where(waste_management: ENVIRONMENT_VAR[:satisfactory])
end

def self.waste_management_poor
	where(waste_management: ENVIRONMENT_VAR[:poor])
end

def self.cantain_and_food_quality_good
	where(cantain_and_food_quality: ENVIRONMENT_VAR[:good])
end

def self.cantain_and_food_quality_satisfactory
	where(cantain_and_food_quality: ENVIRONMENT_VAR[:satisfactory])
end

def self.cantain_and_food_quality_poor
	where(cantain_and_food_quality: ENVIRONMENT_VAR[:poor])
end

def agent_name
	Agent.find_by_id(agent_id).name
end
end
