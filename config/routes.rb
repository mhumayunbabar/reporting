Rails.application.routes.draw do
   get 'admin/login'
  get 'admin/logout'
  get 'admin/settings'
  get 'reports/students'
  get 'reports/schools'
  get 'reports/health_sessions'
  get 'reports/daily_community_visits'
  get 'reports/monthly_school_visits'
  get 'reports/daily_school_visits'
  get 'reports/environment_assessments'

  get 'admin/get_charts'
  get 'admin/update_tiles'
  get 'admin/update_movement_map'
  get 'admin/update_planning_report'
  get 'admin/update_tehsils'
  get 'admin/update_schools'
  get 'admin/update_districts'
  get 'admin/school_details'
  get 'admin/env_assesment_details'
  get 'admin/health_sessions_details'
  get 'admin/std_assesment_details'
  get 'admin/council_meetings_details'
  get 'admin/update_shns'
  get 'admin/update_movement_map'

  # get 'reports/students',  :defaults => { :format => 'xlsx' }
  # get 'reports/schools',  :defaults => { :format => 'xlsx' }
  # get 'reports/health_sessions',  :defaults => { :format => 'xlsx' }
  # get 'reports/environment_assessments',  :defaults => { :format => 'xlsx' }

  post 'admin/session_login'
  post 'admin/save_settings'

  get 'admin/manage_districts'
  get 'admin/manage_divisions'
  get 'admin/manage_ucs'
  get 'admin/edit_division'
  get 'admin/edit_tehsil'
  get 'admin/edit_district'
  get 'admin/edit_uc'
  get 'admin/delete_division'
  get 'admin/delete_district'
  get 'admin/delete_tehsil'
  get 'admin/delete_uc'
  get 'admin/delete_staff'
  get 'admin/manage_tehsils'

  get 'admin/manage_staff'
  get 'admin/manage_shns'
  get 'admin/manage_schools'
  get 'admin/delete_school'
  get 'admin/delete_shns'
  get 'admin/new_staff_member'
  get 'admin/edit_staff'
  get 'admin/new_shns_member'
  get 'admin/export_data'
  get 'admin/community_daily_reporting'
  get 'admin/monthly_school_visits'
  get 'admin/daily_school_visits'
  get 'admin/track_agents'


  get 'admin/division_details'
  get 'admin/district_details'
  get 'admin/tehsil_details'
  get 'admin/update_staff_status'
  get 'admin/school_details'

  post 'admin/create_new_school'
  post 'admin/create_new_shns'
  post 'admin/create_new_staff'
  post 'admin/export_data'
  post 'admin/create_new_district'
  post 'admin/update_district'
  post 'admin/update_staff'
  post 'admin/update_tehsil'
  post 'admin/create_new_division'
  post 'admin/update_division'
  post 'admin/update_uc'
  post 'admin/create_new_uc'
  post 'admin/create_new_tehsil'

  get 'admin/dashboard'
  get 'admin/new_agent'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post 'api/login', :defaults => { :format => 'json' }
  post 'api/agent_location_log', :defaults => { :format => 'json' }
  post 'api/create_student', :defaults => { :format => 'json' }
  post 'api/get_nearby_schools', :defaults => { :format => 'json' }
  get 'api/search_schools_for_select', :defaults => { :format => 'json' }
  get 'api/search_tehsil_for_select', :defaults => { :format => 'json' }
  get 'api/search_districts_for_select', :defaults => { :format => 'json' }
  post 'api/search_schools', :defaults => { :format => 'json' }
  post 'api/search_uc', :defaults => { :format => 'json' }
  post 'api/get_ucs', :defaults => { :format => 'json' }
  post 'api/submit_health_session', :defaults => { :format => 'json' }
  post 'api/submit_child_health_screening', :defaults => { :format => 'json' }
  post 'api/submit_environment_assesment', :defaults => { :format => 'json' }
  post 'api/submit_council_meeting', :defaults => { :format => 'json' }
  post 'api/submit_add_child', :defaults => { :format => 'json' }
  post 'api/submit_planner_record', :defaults => { :format => 'json' }
  post 'api/submit_add_child_health_screening', :defaults => { :format => 'json' }

  get 'graph/students_assessed_by_day', :defaults => { :format => 'json' }
  get 'graph/followups_by_day', :defaults => { :format => 'json' }
  get 'graph/schools_visited_by_day', :defaults => { :format => 'json' }
  get 'graph/health_sessions_by_day', :defaults => { :format => 'json' }
  get 'graph/environmental_assesments_by_day', :defaults => { :format => 'json' }


  root 'admin#login'
# The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
