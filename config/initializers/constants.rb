ROLES_FOR_STAFF = {
	"COS" => 1,
	"DOS" => 2,
	"DOO" => 3,
	"DU" => 4
}


NEAR_DISTACE = 10


API_PARAM_SPLIT_DELI = ';'

TYPES = {
	:school => 0,
	:community => 1
}

TYPE_NAMES = {
	0 => "School",
	1 => "Tehsil"
}

GENDER = {
     0 => "Male",
     1 => "Female"
}


FRIENDLY_NAMES_REPORTS = {

	"grade" => "Grade", 
    "name" => "Name",
    "gaurdian_name" => "Gaurdian Name",
    "condition_of_building" => "Condition Of Building",
    "cleanliness_of_school_environment" => "Cleanliness of School Environment",
    "father_name" => "Father's Name",
    "dob" => "Date of Birth",
    "waste_management" => "Waste Management Present",
    "drainage_condiiton" => "Drainage Condition",
    "siblings" => "Siblings",
    "roof_status" => "Roof Status",
    "class_room_space_adequacy" => "Adequate Space in classrooms",
    "availability_of_safe_drinking_water" => "Availablity of safe drinking water",
    "sanitary_condition_of_toilets" => "Sanitary Condition of Toilets",
    "cantain_and_food_quality" => "Canteen and Food Quality",
    "hieght" => "Height" ,
    "weight" => "Weight" ,
    "lights_present" => "Lights Present" ,
    "last_assessed" => "Last Assessed",
    "support_person_name" => "Support Person name",
    "food_quality" => "Food Quality",
    "support_person_designtion" => "Support Person Designation",
    "bmi" => "BMI",
    "anemia" => "Anemia" ,
    "running_water" => "Running water" ,
    "physical_deformities" => "Pysical Deformities",
    "hearing_problem" =>  "Hearing Problem",
    "ear_problem" => "Ear Problem",
    "vision" => "Vision" ,
    "eye_problem" => "Eye Problem",
    "respiratory_infection" => "Respiratory Infection",
    "skin_problem" => "Skin Problem",
    "trauma_physical_injuries" => "Trauma/ Physical Injuires",
    "clean_water" => "Clean Water",
    "supply" => "Adequate Supply" ,
    "drainage_system" => "Drainage System" ,
    "functional_toilets" => "Functional Toilets" ,
    "toilet_numbers" => "Number of Toilets" ,
    "physical_activity_provisions" => "Physical Activity Provisions" ,
    "soap_water_toilets" => "Soap Water In Toilets" ,
    "soap_container" => "Soap Containers",
    "stagnent_water" => "Stagnent Water" ,
    "drainage_functioning" => "Functioning Drainage" ,
    "waste_managment" => "Waste Management" ,
    "waste_managemnt_freq" => "Waste Management Activity Frequency" ,
    "boundy_wall_gate" => "Boundry Walls And Gate" ,
    "electricity" => "Electricity" ,
    "playground" => "Playground Available" ,
    "clean_class_toilet" => "Clean Classes And Toilets" ,
    "clean_floors" => "Clean Floors" ,
    "janitors" => "Janitors" ,
    "ventilation" => "Ventilation",
    "tables_chairs" => "Tables and Chairs",
    "fans" => "Fans",
    "fountains" => "Fountains",
    "lat" => "Latitude",
    "long" => "Longitude",
    "last_visited" => "Last Visited"

}


MAP_TYPE = {
    "Agent Movement" => 1, "Follow-ups" => 2, 
    "Environment Testing" => 3, "Health Sessions" => 4
}

MAP_TYPE_REV = {
    :agent => 1 ,
    :followups => 2,
    :env => 3,
    :health_sessions => 4
}

ADMIN_STATE = {
    :active => 1,
    :inactive => -1

}


ENVIRONMENT_VAR = {
    :good => 1,
    :satisfactory => 2,
    :poor => 3
}

PLANNER_TYPE ={
    0 => "community",
    1 => "school"
}
